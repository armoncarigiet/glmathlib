//
//  unit_tests.h
//  glmathlib
//
//  Created by Armon Carigiet on 06.10.14.
//
//

#ifndef glmathlib_unit_tests_h
#define glmathlib_unit_tests_h

#include <stddef.h>

#define GLMATH_UNITTEST_STATUS_OK 			1		// Everithing is allright.
#define GLMATH_UNITTEST_STATUS_WARNING		2		// Something could might be wrong.
#define GLMATH_UNITTEST_STATUS_FAILURE		3		// Something has gone wrong.
#define GLMATH_UNITTEST_STATUS_FATAL_ERROR	4		// Something crucial has gone wrong -> abort test.
#define GLMATH_UNITTEST_STATUS_MARK			5		// Just to mark something.

#define GLMATH_UNITTEST_CRITICAL_YES	1
#define GLMATH_UNITTEST_CRITICAL_NO		0

#ifdef __cplusplus
extern "C"{
#endif

typedef int (*glmath_unittest_function)();

struct _glmath_unittest{
    char* name;
	glmath_unittest_function *f;
	int count;
	int critical;
};
typedef struct _glmath_unittest glmath_unittest;

// if critical is set to true unit tests are aborted on failure.
int glmath_unittest_create(glmath_unittest* t, const char* name, int critical, glmath_unittest_function *functions, int functioncount);
void glmath_unittest_destroy(glmath_unittest* t);
int glmath_unittest_run(glmath_unittest* t);

int glmath_unittest_log(const char* msg, int status, const char* file, int line);

//#define glmath_unittest_log_ok(msg) glmath_unittest_log(""#msg, GLMATH_UNITTEST_STATUS_OK, __FILE__, __LINE__);
//#define glmath_unittest_log_warning(msg) glmath_unittest_log(""#msg, GLMATH_UNITTEST_STATUS_WARNING, __FILE__, __LINE__);
//#define glmath_unittest_log_failure(msg) glmath_unittest_log(""#msg, GLMATH_UNITTEST_STATUS_FAILURE, __FILE__, __LINE__);
//#define glmath_unittest_log_fatal_error(msg) glmath_unittest_log(""#msg, GLMATH_UNITTEST_STATUS_FATAL_ERROR, __FILE__, __LINE__);
//
	
#ifdef __cplusplus
}
#endif

#define glmath_unittest_assert_msg(expr, msg) expr ? glmath_unittest_log(msg, GLMATH_UNITTEST_STATUS_OK, __FILE__, __LINE__) : glmath_unittest_log(msg, GLMATH_UNITTEST_STATUS_FAILURE, __FILE__, __LINE__);
#define glmath_unittest_assert(expr) expr ? glmath_unittest_log("Assertation", GLMATH_UNITTEST_STATUS_OK, __FILE__, __LINE__) : glmath_unittest_log("Assertation", GLMATH_UNITTEST_STATUS_FAILURE, __FILE__, __LINE__);
#define glmath_unittest_assert_warining(expr) expr ? glmath_unittest_log("Assertation", GLMATH_UNITTEST_STATUS_OK, __FILE__, __LINE__) : glmath_unittest_log("Assertation", GLMATH_UNITTEST_STATUS_WARNING, __FILE__, __LINE__);
#define glmath_unittest_assert_abort(expr) expr ? glmath_unittest_log("Assertation", GLMATH_UNITTEST_STATUS_OK, __FILE__, __LINE__) : glmath_unittest_log("Assertation", GLMATH_UNITTEST_STATUS_FATAL_ERROR, __FILE__, __LINE__);

#endif
