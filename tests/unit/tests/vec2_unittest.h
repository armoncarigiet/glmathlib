//
//  vec2_unittest.h
//  glmathlib
//
//  Created by Armon Carigiet on 07.10.14.
//
//

#ifndef glmathlib_vec2_unittest_h
#define glmathlib_vec2_unittest_h

#include "../unit_tests.h"
#include "../../../include/glmath/glmath_vec2.h"

void glmath_unittest_vec2_run();

#endif
