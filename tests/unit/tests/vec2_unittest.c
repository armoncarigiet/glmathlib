//
//  vec2_unittest.c
//  glmathlib
//
//  Created by Armon Carigiet on 07.10.14.
//
//

#include "vec2_unittest.h"

int test_vec2_lerp(){
	glmath_vec2 v1=glmath_vec2_create(4, 5);
	glmath_vec2 v2=glmath_vec2_create(1, 2);
	glmath_vec2 v=glmath_vec2_lerp(v2, v1, 1);
	int r=0;
	if(v.v[0]==v1.v[0] && v.v[1]==v1.v[1]){
		r=1;
	}
	return glmath_unittest_assert_msg(r, "Lerp identity")
};

void glmath_unittest_vec2_run(){
	int count=1;
	glmath_unittest u;
	glmath_unittest_function f[count];
	f[0]=test_vec2_lerp;
	glmath_unittest_create(&u, "Test vec2", 0, f, count);
	glmath_unittest_run(&u);
}
