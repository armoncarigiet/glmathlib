//
//  unit_tests.c
//  glmathlib
//
//  Created by Armon Carigiet on 06.10.14.
//
//

#include "unit_tests.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

int glmath_unittest_create(glmath_unittest* t, const char* name, int critical, glmath_unittest_function *functions, int functioncount){
	t->count=functioncount;
	t->f=functions;
	t->name=malloc(strlen((const char*)name)*sizeof(char));
	if(!t->name){
		free(t->name);
		return 0;
	}
	strcpy(t->name, (const char*)name);
	t->critical=critical;
	return 1;
}
void glmath_unittest_destroy(glmath_unittest* t){
	free(t->name);
}

int glmath_unittest_run(glmath_unittest* t){
	printf("\nUnittest: %s\n\n", t->name);
	int warnings=0;
	int errors=0;
	int fatal_error=0;
	int aborted=0;
	for(int i=0; i<t->count; i++){
		int status=t->f[i]();
		if(status==GLMATH_UNITTEST_STATUS_WARNING){
			warnings++;
		}else if(status==GLMATH_UNITTEST_STATUS_FAILURE){
			errors++;
			if(t->critical){
				aborted=1;
				break;
			}
		}else if(status==GLMATH_UNITTEST_STATUS_FATAL_ERROR){
			fatal_error=1;
			errors++;
			aborted=1;
			break;
		}
	}
	if(aborted){
		printf("\nUnittest aborted");
		if(fatal_error){
			printf(": fatal error detected!");
			exit(-1);
		}
	}else{
		printf("\nUnittest done");
	}
	
	printf("\nWarnings: %i\nErrors: %i\n\n", warnings, errors);
	return 1;
}

int glmath_unittest_log(const char* msg, int status, const char* file, int line){
	if (status==GLMATH_UNITTEST_STATUS_OK){
		printf("%s\t%s\n", msg, "[ OK ]");
	}else if(status==GLMATH_UNITTEST_STATUS_WARNING){
		printf("%s\t%s\n\t file:\"%s\"\n\t line:%i\n", msg, "[ Warning ]", file, line);
	}else if(status==GLMATH_UNITTEST_STATUS_FAILURE){
		printf("%s\t%s\n\t file:\"%s\"\n\t line:%i\n", msg, "[ FAILURE ] !", file, line);
	}else if(status==GLMATH_UNITTEST_STATUS_FATAL_ERROR){
		printf("%s\t%s\n\t file:\"%s\"\n\t line:%i\n", msg, "[ FATAL ERROR ] !!!", file, line);
	}
	return status;
}
