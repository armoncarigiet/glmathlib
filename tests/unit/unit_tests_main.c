//
//  main.c
//  glmathlibtests
//
//  Created by Armon Carigiet on 06.10.14.
//
//

#include <stdio.h>
#include "unit_tests.h"

#include "../../include/glmath/glmath_types.h"
#include "../../include/glmath/glmath_vec2.h"
#include "../../include/glmath/glmath_vec3.h"
#include "../../include/glmath/glmath_vec4.h"
#include "../../include/glmath/glmath_mat2.h"
#include "../../include/glmath/glmath_mat3.h"
#include "../../include/glmath/glmath_mat4.h"
#include "../../include/glmath/glmath_quat.h"

#include "tests/vec2_unittest.h"



int main(int argc, const char * argv[])
{
	printf("%lu",sizeof(long double));
	//glmath_unittest_vec2_run();
	
    return 0;
}

