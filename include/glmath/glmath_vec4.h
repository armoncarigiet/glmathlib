//===========================================================================
/*
 *  glmath - OpenGL Mathematics Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file glmath_vec4.h
 *	@date 10.10.2014
 *  @author Armon Carigiet
 *
 *  @brief Provides algebra and transformation functions for 4d vectors (glmath_vec4).
 *  @see glmath_types.h
 */
//===========================================================================

#ifndef glmathlib_glmath_vec4_h
#define glmathlib_glmath_vec4_h

#include "glmath_types.h"

#ifdef __cplusplus
extern "C" {
#endif

//! Builds a vector(3) from vector components.
//! @note w component should be 1 (for positions) or 0 (for directions)
//! @param x x vector component
//! @param y y vector component
//! @param z z vector component
//! @param w w vector component
//! @return vector
static inline glmath_vec4 glmath_vec4_create(glmath_real x, glmath_real y, glmath_real z, glmath_real w);

//! Negates a vector(4).
//! @param v input vector
//! @return negated vector
static inline glmath_vec4 glmath_vec4_neg(glmath_vec4 v);

//! Adds two vectors(4).
//! @param v1 left vector
//! @param v2 right vector
//! @return resulting vector
static inline glmath_vec4 glmath_vec4_add(glmath_vec4 v1, glmath_vec4 v2);

//! Subtracts two vectors(4).
//! @param v1 left vector
//! @param v2 right vector
//! @return resulting vector
static inline glmath_vec4 glmath_vec4_sub(glmath_vec4 v1, glmath_vec4 v2);

//! Multiplicates two vectors(4).
//! @param v1 left vector
//! @param v2 right vector
//! @return resulting vector
static inline glmath_vec4 glmath_vec4_mul(glmath_vec4 v1, glmath_vec4 v2);

//! Divides two vectors(4).
//! @param v1 left vector
//! @param v2 right vector
//! @return resulting vector
static inline glmath_vec4 glmath_vec4_div(glmath_vec4 v1, glmath_vec4 v2);

//! Adds a scalar value to a vector(4).
//! @param v vector
//! @param a scalar value
//! @return resulting vector
static inline glmath_vec4 glmath_vec4_add_s(glmath_vec4 v, glmath_real a);

//! Subtracts a scalar value to a vector(4).
//! @param v vector
//! @param a scalar value
//! @return resulting vector
static inline glmath_vec4 glmath_vec4_sub_s(glmath_vec4 v, glmath_real a);

//! Multiplicates a vector(4) by a sclar value.
//! @param v vector
//! @param a scalar value
//! @return resulting vector
static inline glmath_vec4 glmath_vec4_mul_s(glmath_vec4 v, glmath_real a);

//! Divides a vector(4) by a sclar value.
//! @param v vector
//! @param a scalar value
//! @return resulting vector
static inline glmath_vec4 glmath_vec4_div_s(glmath_vec4 v, glmath_real a);



//! Calculates the magnitude of a vector(4).
//! @param v vector
//! @return magnitude
static inline glmath_real glmath_vec4_mag(glmath_vec4 v);

//! Calculates the distance between two vectors(4).
//! @param v1 first vector
//! @param v2 second vector
//! @return distance
static inline glmath_real glmath_vec4_dist(glmath_vec4 v1, glmath_vec4 v2);

//! Normalizes a vector(4).
//! @param v vector
//! @return normalized vector
static inline glmath_vec4 glmath_vec4_norm(glmath_vec4 v);

//! Calculates the dot-product of two vectors(4).
//! @param v1 first vector
//! @param v2 second vector
//! @return dot product
static inline glmath_real glmath_vec4_dot(glmath_vec4 v1, glmath_vec4 v2);

//! Calculates the cross-product of two vectors(4).
//! @param v1 left vector
//! @param v2 right vector
//! @return resulting vecor
static inline glmath_vec4 glmath_vec4_cross(glmath_vec4 v1, glmath_vec4 v2);

//! Compares two vectors(4) and creates new one with the bigger components.
//! @param v1 first vector
//! @param v2 second vector
//! @return resulting vector
static inline glmath_vec4 glmath_vec4_max(glmath_vec4 v1, glmath_vec4 v2);

//! Compares two vectors(4) and creates new one with the smaller components.
//! @param v1 first vector
//! @param v2 second vector
//! @return resulting vector
static inline glmath_vec4 glmath_vec4_min(glmath_vec4 v1, glmath_vec4 v2);

//! Interpolates linear two vectors(4).
//! @note a should be in the interval ]0,1[
//! @param sv start vector
//! @param ev end vector
//! @param a interpolation skalar
//! @return resulting vector
static inline glmath_vec4 glmath_vec4_lerp(glmath_vec4 sv, glmath_vec4 ev, glmath_real a);

//! Projects a vector(4) onto another.
//! @param v vector to project
//! @param pv projection vector
//! @return projected vector
static inline glmath_vec4 glmath_vec4_proj(glmath_vec4 v, glmath_vec4 pv);

#include "glmath_vec4.inl"

#ifdef __cplusplus
}
#endif

#endif