//===========================================================================
/*
 *  glmath - OpenGL Mathematics Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file glmath_mat4.inl
 *	@date 17.03.14.
 *  @author Armon Carigiet
 *  @see glmath_mat4.h
 */
//===========================================================================

// TODO fix alignment problems


static inline glmath_mat4 glmath_mat4_create_identity(){
	glmath_mat4 m={{
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	}};
	return m;
}

static inline glmath_mat4 glmath_mat4_create(glmath_real m00, glmath_real m01, glmath_real m02, glmath_real m03, glmath_real m10, glmath_real m11, glmath_real m12, glmath_real m13, glmath_real m20, glmath_real m21, glmath_real m22, glmath_real m23, glmath_real m30, glmath_real m31, glmath_real m32, glmath_real m33){
	glmath_mat4 m={{
		m00, m01, m02, m03,
		m10, m11, m12, m13,
		m20, m21, m22, m23,
		m30, m31, m32, m33
	}};
	return m;
}

static inline glmath_mat4 glmath_mat4_create_quat(glmath_quat q){
	q=glmath_quat_norm(q);
	glmath_real x2=q.q[1]*q.q[1];
	glmath_real y2=q.q[2]*q.q[2];
	glmath_real z2=q.q[3]*q.q[3];
	
//	glmath_mat4 mat={{
//		1-2*(y2+z2), 2*(q.q[1]*q.q[2] - q.q[0]*q.q[3]), 2*(q.q[1]*q.q[3] + q.q[0]*q.q[2]), 0.0f,
//		2*(q.q[1]*q.q[2] + q.q[0]*q.q[3]), 1-2*(x2+z2), 2*(q.q[2]*q.q[3] - q.q[0]*q.q[1]), 0.0f,
//		2*(q.q[1]*q.q[3] - q.q[0]*q.q[2]), 2*(q.q[2]*q.q[3] + q.q[0]*q.q[1]), 1-2*(x2+y2), 0.0f,
//		0.0f, 0.0f, 0.0f, 1.0f
//	}};
	
	glmath_mat4 mat={{
		1-2*(y2+z2), 2*(q.q[1]*q.q[2] + q.q[0]*q.q[3]), 2*(q.q[1]*q.q[3] - q.q[0]*q.q[2]), 0.0f,
		2*(q.q[1]*q.q[2] - q.q[0]*q.q[3]), 1-2*(x2+z2), 2*(q.q[2]*q.q[3] + q.q[0]*q.q[1]), 0.0f,
		2*(q.q[1]*q.q[3] + q.q[0]*q.q[2]), 2*(q.q[2]*q.q[3] - q.q[0]*q.q[1]), 1-2*(x2+y2), 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	}};
	return mat;
}

static inline glmath_mat3 glmath_mat4_get_mat3(glmath_mat4 m){
	glmath_mat3 mr={{
		m.m00, m.m01, m.m02,
		m.m10, m.m11, m.m12,
		m.m20, m.m21, m.m22
	}};
	return mr;
}

static inline glmath_mat4 glmath_mat4_transpose(glmath_mat4 m){
	glmath_mat4 m1={{
		m.m[0], m.m[4], m.m[8], m.m[12],
		m.m[1], m.m[5], m.m[9], m.m[13],
		m.m[2], m.m[6], m.m[10], m.m[14],
		m.m[3], m.m[7], m.m[11], m.m[15]
	}};
	return m1;
}

static inline glmath_real glmath_mat4_det(glmath_mat4 m){
	glmath_real f;
	
	glmath_mat3 m0={{
		m.m[5], m.m[6], m.m[7],
		m.m[9], m.m[10], m.m[11],
		m.m[13], m.m[14], m.m[15]
	}};
	glmath_mat3 m1={{
		m.m[4], m.m[6], m.m[7],
		m.m[8], m.m[10], m.m[11],
		m.m[12], m.m[14], m.m[15]
	}};
	glmath_mat3 m2={{
		m.m[4], m.m[5], m.m[7],
		m.m[8], m.m[9], m.m[11],
		m.m[12], m.m[13], m.m[15]
	}};
	glmath_mat3 m3={{
		m.m[4], m.m[5], m.m[6],
		m.m[8], m.m[9], m.m[10],
		m.m[12], m.m[13], m.m[14]
	}};
	
	f=m.m[0]*glmath_mat3_det(m0) - m.m[1]*glmath_mat3_det(m1) + m.m[2]*glmath_mat3_det(m2) - m.m[3]*glmath_mat3_det(m3);
	
	return f;
}

static inline glmath_mat4 glmath_mat4_invert(glmath_mat4 m, bool* invertable){
	glmath_real inv=true;
	glmath_real det=glmath_mat4_det(m);
	glmath_real tmp=0.0f;
	glmath_mat4 mr=glmath_mat4_create_identity();
	//#ifdef __SSE3__
	// TODO
	//#else
	if(det==0){
		inv=false;
	}else{
		//adjustments
		if(m.m[0]==0){
			if(m.m[4]==0){
				if (m.m[8]==0){
					if (m.m[12]==0) {
						inv=false;
					}else{
						tmp=m.m[0];
						m.m[0]=m.m[12];
						m.m[12]=tmp;
						
						tmp=m.m[1];
						m.m[1]=m.m[13];
						m.m[13]=tmp;
						
						tmp=m.m[2];
						m.m[2]=m.m[14];
						m.m[14]=tmp;
						
						tmp=m.m[3];
						m.m[3]=m.m[15];
						m.m[15]=tmp;
						
						tmp=mr.m[0];
						mr.m[0]=mr.m[8];
						mr.m[8]=tmp;
						
						tmp=mr.m[3];
						mr.m[3]=mr.m[15];
						mr.m[15]=tmp;
					}
				}else{
					tmp=m.m[0];
					m.m[0]=m.m[8];
					m.m[8]=tmp;
					
					tmp=m.m[1];
					m.m[1]=m.m[9];
					m.m[9]=tmp;
					
					tmp=m.m[2];
					m.m[2]=m.m[10];
					m.m[10]=tmp;
					
					tmp=m.m[3];
					m.m[3]=m.m[11];
					m.m[11]=tmp;
					
					tmp=mr.m[0];
					mr.m[0]=mr.m[8];
					mr.m[8]=tmp;
					
					tmp=mr.m[2];
					mr.m[2]=mr.m[10];
					mr.m[10]=tmp;
				}
			}else{
				tmp=m.m[0];
				m.m[0]=m.m[4];
				m.m[4]=tmp;
				
				tmp=m.m[1];
				m.m[1]=m.m[5];
				m.m[5]=tmp;
				
				tmp=m.m[2];
				m.m[2]=m.m[6];
				m.m[6]=tmp;
				
				tmp=m.m[3];
				m.m[3]=m.m[7];
				m.m[7]=tmp;
				
				tmp=mr.m[0];
				mr.m[0]=mr.m[4];
				mr.m[4]=tmp;
				
				tmp=mr.m[1];
				mr.m[1]=mr.m[5];
				mr.m[5]=tmp;
			}
		}
		if(inv){
			if(m.m[5]==0){
				if(m.m[9]==0){
					if(m.m[13]==0) {
						if(m.m[1]==0){
							inv=false;
						}else{
							tmp=m.m[4];
							m.m[4]=m.m[0];
							m.m[0]=tmp;
							
							tmp=m.m[5];
							m.m[5]=m.m[1];
							m.m[1]=tmp;
							
							tmp=m.m[6];
							m.m[6]=m.m[2];
							m.m[2]=tmp;
							
							tmp=m.m[7];
							m.m[7]=m.m[3];
							m.m[3]=tmp;
							
							tmp=mr.m[4];
							mr.m[4]=mr.m[0];
							mr.m[0]=tmp;
							
							tmp=mr.m[5];
							mr.m[5]=mr.m[1];
							mr.m[1]=tmp;
							
							tmp=mr.m[6];
							mr.m[6]=mr.m[2];
							mr.m[2]=tmp;
							
							tmp=mr.m[7];
							mr.m[7]=mr.m[3];
							mr.m[3]=tmp;
						}
					}else{
						tmp=m.m[4];
						m.m[4]=m.m[12];
						m.m[12]=tmp;
						
						tmp=m.m[5];
						m.m[5]=m.m[13];
						m.m[13]=tmp;
						
						tmp=m.m[6];
						m.m[6]=m.m[14];
						m.m[14]=tmp;
						
						tmp=m.m[7];
						m.m[7]=m.m[15];
						m.m[15]=tmp;
						
						tmp=mr.m[4];
						mr.m[4]=mr.m[12];
						mr.m[12]=tmp;
						
						tmp=mr.m[5];
						mr.m[5]=mr.m[13];
						mr.m[13]=tmp;
						
						tmp=mr.m[6];
						mr.m[6]=mr.m[14];
						mr.m[14]=tmp;
						
						tmp=mr.m[7];
						mr.m[7]=mr.m[15];
						mr.m[15]=tmp;
					}
				}else{
					tmp=m.m[4];
					m.m[4]=m.m[8];
					m.m[8]=tmp;
					
					tmp=m.m[5];
					m.m[5]=m.m[9];
					m.m[9]=tmp;
					
					tmp=m.m[6];
					m.m[6]=m.m[10];
					m.m[10]=tmp;
					
					tmp=m.m[7];
					m.m[7]=m.m[11];
					m.m[11]=tmp;
					
					tmp=mr.m[4];
					mr.m[4]=mr.m[8];
					mr.m[8]=tmp;
					
					tmp=mr.m[5];
					mr.m[5]=mr.m[9];
					mr.m[9]=tmp;
					
					tmp=mr.m[6];
					mr.m[6]=mr.m[10];
					mr.m[10]=tmp;
					
					tmp=mr.m[7];
					mr.m[7]=mr.m[11];
					mr.m[11]=tmp;
				}
			}
			if(inv){
				if(m.m[10]==0){
					if(m.m[14]==0){
						if (m.m[2]==0){
							if (m.m[6]==0) {
								inv=false;
							}else{
								tmp=m.m[8];
								m.m[8]=m.m[4];
								m.m[4]=tmp;
								
								tmp=m.m[9];
								m.m[9]=m.m[5];
								m.m[5]=tmp;
								
								tmp=m.m[10];
								m.m[10]=m.m[6];
								m.m[6]=tmp;
								
								tmp=m.m[11];
								m.m[11]=m.m[7];
								m.m[7]=tmp;
								
								tmp=mr.m[8];
								mr.m[8]=mr.m[4];
								mr.m[3]=tmp;
								
								tmp=mr.m[9];
								mr.m[9]=mr.m[5];
								mr.m[5]=tmp;
								
								tmp=mr.m[10];
								mr.m[10]=mr.m[6];
								mr.m[6]=tmp;
								
								tmp=mr.m[11];
								mr.m[11]=mr.m[7];
								mr.m[7]=tmp;
							}
						}else{
							tmp=m.m[8];
							m.m[8]=m.m[12];
							m.m[12]=tmp;
							
							tmp=m.m[9];
							m.m[9]=m.m[13];
							m.m[13]=tmp;
							
							tmp=m.m[10];
							m.m[10]=m.m[14];
							m.m[14]=tmp;
							
							tmp=m.m[11];
							m.m[11]=m.m[15];
							m.m[15]=tmp;
							
							tmp=mr.m[8];
							mr.m[8]=mr.m[12];
							mr.m[12]=tmp;
							
							tmp=mr.m[9];
							mr.m[9]=mr.m[13];
							mr.m[13]=tmp;
							
							tmp=mr.m[10];
							mr.m[10]=mr.m[14];
							mr.m[14]=tmp;
							
							tmp=mr.m[11];
							mr.m[11]=mr.m[15];
							mr.m[15]=tmp;
						}
					}else{
						tmp=m.m[8];
						m.m[8]=m.m[0];
						m.m[0]=tmp;
						
						tmp=m.m[9];
						m.m[9]=m.m[1];
						m.m[1]=tmp;
						
						tmp=m.m[10];
						m.m[10]=m.m[2];
						m.m[2]=tmp;
						
						tmp=m.m[11];
						m.m[11]=m.m[3];
						m.m[3]=tmp;
						
						tmp=mr.m[8];
						mr.m[8]=mr.m[0];
						mr.m[0]=tmp;
						
						tmp=mr.m[9];
						mr.m[9]=mr.m[1];
						mr.m[1]=tmp;
						
						tmp=mr.m[10];
						mr.m[10]=mr.m[2];
						mr.m[2]=tmp;
						
						tmp=mr.m[11];
						mr.m[11]=mr.m[3];
						mr.m[3]=tmp;
					}
				}
				if(inv){
					if(m.m[15]==0){
						if(m.m[3]==0){
							if (m.m[7]==0){
								if (m.m[11]==0) {
									inv=false;
								}else{
									tmp=m.m[12];
									m.m[12]=m.m[8];
									m.m[8]=tmp;
									
									tmp=m.m[13];
									m.m[13]=m.m[9];
									m.m[9]=tmp;
									
									tmp=m.m[14];
									m.m[14]=m.m[10];
									m.m[10]=tmp;
									
									tmp=m.m[15];
									m.m[15]=m.m[11];
									m.m[11]=tmp;
									
									tmp=mr.m[12];
									mr.m[12]=mr.m[8];
									mr.m[8]=tmp;
									
									tmp=mr.m[13];
									mr.m[13]=mr.m[9];
									mr.m[9]=tmp;
									
									tmp=mr.m[14];
									mr.m[14]=mr.m[10];
									mr.m[10]=tmp;
									
									tmp=mr.m[15];
									mr.m[15]=mr.m[11];
									mr.m[11]=tmp;
								}
							}else{
								tmp=m.m[12];
								m.m[12]=m.m[4];
								m.m[4]=tmp;
								
								tmp=m.m[13];
								m.m[13]=m.m[5];
								m.m[5]=tmp;
								
								tmp=m.m[14];
								m.m[14]=m.m[6];
								m.m[6]=tmp;
								
								tmp=m.m[15];
								m.m[15]=m.m[7];
								m.m[7]=tmp;
								
								tmp=mr.m[12];
								mr.m[12]=mr.m[4];
								mr.m[4]=tmp;
								
								tmp=mr.m[13];
								mr.m[13]=mr.m[5];
								mr.m[5]=tmp;
								
								tmp=mr.m[14];
								mr.m[14]=mr.m[6];
								mr.m[6]=tmp;
								
								tmp=mr.m[15];
								mr.m[15]=mr.m[7];
								mr.m[7]=tmp;
							}
						}else{
							tmp=m.m[12];
							m.m[12]=m.m[0];
							m.m[0]=tmp;
							
							tmp=m.m[13];
							m.m[13]=m.m[1];
							m.m[1]=tmp;
							
							tmp=m.m[14];
							m.m[14]=m.m[2];
							m.m[2]=tmp;
							
							tmp=m.m[15];
							m.m[15]=m.m[3];
							m.m[3]=tmp;
							
							tmp=mr.m[12];
							mr.m[12]=mr.m[0];
							mr.m[0]=tmp;
							
							tmp=mr.m[13];
							mr.m[13]=mr.m[1];
							mr.m[1]=tmp;
							
							tmp=mr.m[14];
							mr.m[14]=mr.m[2];
							mr.m[2]=tmp;
							
							tmp=mr.m[15];
							mr.m[15]=mr.m[3];
							mr.m[3]=tmp;
						}
					}
					//#ifdef __SSE3__
					// TODO
					//#else
					//#endif
					if(inv){
						//calculation
						tmp=m.m[0];
						m.m[0]=m.m[0]/tmp;
						m.m[1]=m.m[1]/tmp;
						m.m[2]=m.m[2]/tmp;
						m.m[3]=m.m[3]/tmp;
						
						mr.m[0]=mr.m[0]/tmp;
						mr.m[1]=mr.m[1]/tmp;
						mr.m[2]=mr.m[2]/tmp;
						mr.m[3]=mr.m[3]/tmp;
						
						tmp=m.m[4];
						m.m[4]=m.m[4] - tmp*m.m[0];
						m.m[5]=m.m[5] - tmp*m.m[1];
						m.m[6]=m.m[6] - tmp*m.m[2];
						m.m[7]=m.m[7] - tmp*m.m[3];
						
						mr.m[4]=mr.m[4] - tmp*mr.m[0];
						mr.m[5]=mr.m[5] - tmp*mr.m[1];
						mr.m[6]=mr.m[6] - tmp*mr.m[2];
						mr.m[7]=mr.m[7] - tmp*mr.m[3];
						
						tmp=m.m[8];
						m.m[8]=m.m[8] - tmp*m.m[0];
						m.m[9]=m.m[9] - tmp*m.m[1];
						m.m[10]=m.m[10] - tmp*m.m[2];
						m.m[11]=m.m[11] - tmp*m.m[3];
						
						mr.m[8]=mr.m[8] - tmp*mr.m[0];
						mr.m[9]=mr.m[9] - tmp*mr.m[1];
						mr.m[10]=mr.m[10] - tmp*mr.m[2];
						mr.m[11]=mr.m[11] - tmp*mr.m[3];
						
						tmp=m.m[12];
						m.m[12]=m.m[12] - tmp*m.m[0];
						m.m[13]=m.m[13] - tmp*m.m[1];
						m.m[14]=m.m[14] - tmp*m.m[2];
						m.m[15]=m.m[15] - tmp*m.m[3];
						
						mr.m[12]=mr.m[12] - tmp*mr.m[0];
						mr.m[13]=mr.m[13] - tmp*mr.m[1];
						mr.m[14]=mr.m[14] - tmp*mr.m[2];
						mr.m[15]=mr.m[15] - tmp*mr.m[3];
						
						tmp=m.m[5];
						m.m[4]=m.m[4]/tmp;
						m.m[5]=m.m[5]/tmp;
						m.m[6]=m.m[6]/tmp;
						m.m[7]=m.m[7]/tmp;
						
						mr.m[4]=mr.m[4]/tmp;
						mr.m[5]=mr.m[5]/tmp;
						mr.m[6]=mr.m[6]/tmp;
						mr.m[7]=mr.m[7]/tmp;
						
						tmp=m.m[1];
						m.m[0]=m.m[0] - tmp*m.m[4];
						m.m[1]=m.m[1] - tmp*m.m[5];
						m.m[2]=m.m[2] - tmp*m.m[6];
						m.m[3]=m.m[3] - tmp*m.m[7];
						
						mr.m[0]=mr.m[0] - tmp*mr.m[4];
						mr.m[1]=mr.m[1] - tmp*mr.m[5];
						mr.m[2]=mr.m[2] - tmp*mr.m[6];
						mr.m[3]=mr.m[3] - tmp*mr.m[7];
						
						tmp=m.m[9];
						m.m[8]=m.m[8] - tmp*m.m[4];
						m.m[9]=m.m[9] - tmp*m.m[5];
						m.m[10]=m.m[10] - tmp*m.m[6];
						m.m[11]=m.m[11] - tmp*m.m[7];
						
						mr.m[8]=mr.m[8] - tmp*mr.m[4];
						mr.m[9]=mr.m[9] - tmp*mr.m[5];
						mr.m[10]=mr.m[10] - tmp*mr.m[6];
						mr.m[11]=mr.m[11] - tmp*mr.m[7];
						
						tmp=m.m[13];
						m.m[12]=m.m[12] - tmp*m.m[4];
						m.m[13]=m.m[13] - tmp*m.m[5];
						m.m[14]=m.m[14] - tmp*m.m[6];
						m.m[15]=m.m[15] - tmp*m.m[7];
						
						mr.m[12]=mr.m[12] - tmp*mr.m[4];
						mr.m[13]=mr.m[13] - tmp*mr.m[5];
						mr.m[14]=mr.m[14] - tmp*mr.m[6];
						mr.m[15]=mr.m[15] - tmp*mr.m[7];
						
						
						tmp=m.m[10];
						m.m[8]=m.m[8]/tmp;
						m.m[9]=m.m[9]/tmp;
						m.m[10]=m.m[10]/tmp;
						m.m[11]=m.m[11]/tmp;
						
						mr.m[8]=mr.m[8]/tmp;
						mr.m[9]=mr.m[9]/tmp;
						mr.m[10]=mr.m[10]/tmp;
						mr.m[11]=mr.m[11]/tmp;
						
						tmp=m.m[2];
						m.m[0]=m.m[0] - tmp*m.m[8];
						m.m[1]=m.m[1] - tmp*m.m[9];
						m.m[2]=m.m[2] - tmp*m.m[10];
						m.m[3]=m.m[3] - tmp*m.m[11];
						
						mr.m[0]=mr.m[0] - tmp*mr.m[8];
						mr.m[1]=mr.m[1] - tmp*mr.m[9];
						mr.m[2]=mr.m[2] - tmp*mr.m[10];
						mr.m[3]=mr.m[3] - tmp*mr.m[11];
						
						tmp=m.m[6];
						m.m[4]=m.m[4] - tmp*m.m[8];
						m.m[5]=m.m[5] - tmp*m.m[9];
						m.m[6]=m.m[6] - tmp*m.m[10];
						m.m[7]=m.m[7] - tmp*m.m[11];
						
						mr.m[4]=mr.m[4] - tmp*mr.m[8];
						mr.m[5]=mr.m[5] - tmp*mr.m[9];
						mr.m[6]=mr.m[6] - tmp*mr.m[10];
						mr.m[7]=mr.m[7] - tmp*mr.m[11];
						
						tmp=m.m[14];
						m.m[12]=m.m[12] - tmp*m.m[8];
						m.m[13]=m.m[13] - tmp*m.m[9];
						m.m[14]=m.m[14] - tmp*m.m[10];
						m.m[15]=m.m[15] - tmp*m.m[11];
						
						mr.m[12]=mr.m[12] - tmp*mr.m[8];
						mr.m[13]=mr.m[13] - tmp*mr.m[9];
						mr.m[14]=mr.m[14] - tmp*mr.m[10];
						mr.m[15]=mr.m[15] - tmp*mr.m[11];
						
						tmp=m.m[15];
						m.m[12]=m.m[12]/tmp;
						m.m[13]=m.m[13]/tmp;
						m.m[14]=m.m[14]/tmp;
						m.m[15]=m.m[15]/tmp;
						
						mr.m[12]=mr.m[12]/tmp;
						mr.m[13]=mr.m[13]/tmp;
						mr.m[14]=mr.m[14]/tmp;
						mr.m[15]=mr.m[15]/tmp;
						
						tmp=m.m[3];
						m.m[0]=m.m[0] - tmp*m.m[12];
						m.m[1]=m.m[1] - tmp*m.m[13];
						m.m[2]=m.m[2] - tmp*m.m[14];
						m.m[3]=m.m[3] - tmp*m.m[15];
						
						mr.m[0]=mr.m[0] - tmp*mr.m[12];
						mr.m[1]=mr.m[1] - tmp*mr.m[13];
						mr.m[2]=mr.m[2] - tmp*mr.m[14];
						mr.m[3]=mr.m[3] - tmp*mr.m[15];
						
						tmp=m.m[7];
						m.m[4]=m.m[4] - tmp*m.m[12];
						m.m[5]=m.m[5] - tmp*m.m[13];
						m.m[6]=m.m[6] - tmp*m.m[14];
						m.m[7]=m.m[7] - tmp*m.m[15];
						
						mr.m[4]=mr.m[4] - tmp*mr.m[12];
						mr.m[5]=mr.m[5] - tmp*mr.m[13];
						mr.m[6]=mr.m[6] - tmp*mr.m[14];
						mr.m[7]=mr.m[7] - tmp*mr.m[15];
						
						tmp=m.m[11];
						m.m[8]=m.m[8] - tmp*m.m[12];
						m.m[9]=m.m[9] - tmp*m.m[13];
						m.m[10]=m.m[10] - tmp*m.m[14];
						m.m[11]=m.m[11] - tmp*m.m[15];
						
						mr.m[8]=mr.m[8] - tmp*mr.m[12];
						mr.m[9]=mr.m[9] - tmp*mr.m[13];
						mr.m[10]=mr.m[10] - tmp*mr.m[14];
						mr.m[11]=mr.m[11] - tmp*mr.m[15];
					}
				}
			}
		}
	}
	
	//#endif
	
//	if(inv==false){
//		mr.m[0]=NAN;
//		mr.m[1]=NAN;
//		mr.m[2]=NAN;
//		mr.m[3]=NAN;
//		mr.m[4]=NAN;
//		mr.m[5]=NAN;
//		mr.m[6]=NAN;
//		mr.m[7]=NAN;
//		mr.m[8]=NAN;
//	}
	*invertable=inv;
	return mr;
}

static inline glmath_mat4 glmath_mat4_create_translation(glmath_real x, glmath_real y, glmath_real z){
	glmath_mat4 m={{
		1.0f, 0, 0, 0,
		0, 1.0f, 0, 0,
		0, 0, 1.0f, 0,
		x, y, z, 1.0f
	}};
	return m;
}

static inline glmath_mat4 glmath_mat4_create_translation_vec3(glmath_vec3 v){
	glmath_mat4 m={{
		1.0f, 0, 0, 0,
		0, 1.0f, 0, 0,
		0, 0, 1.0f, 0,
		v.v[0], v.v[1], v.v[2], 1.0f
	}};
	return m;
}

static inline glmath_mat4 glmath_mat4_create_translation_vec4(glmath_vec4 v){
	glmath_mat4 m={{
		1.0f, 0, 0, 0,
		0, 1.0f, 0, 0,
		0, 0, 1.0f, 0,
		v.v[0], v.v[1], v.v[2], 1.0f
	}};
	return m;
}

static inline glmath_mat4 glmath_mat4_translate(glmath_mat4 m, glmath_real x, glmath_real y, glmath_real z){
	glmath_real tx=m.m[0] * x + m.m[4] * y + m.m[8] * z + m.m[12];
	glmath_real ty=m.m[1] * x + m.m[5] * y + m.m[9] * z + m.m[13];
	glmath_real tz=m.m[2] * x + m.m[6] * y + m.m[10] * z + m.m[14];
	glmath_mat4 mr={{
		m.m[0], m.m[1], m.m[2], m.m[3],
		m.m[4], m.m[5], m.m[6], m.m[7],
		m.m[8], m.m[9], m.m[10], m.m[11],
		tx, ty, tz, m.m[15]
	}};
	return mr;
}

static inline glmath_mat4 glmath_mat4_translate_vec3(glmath_mat4 m, glmath_vec3 v){
	glmath_real tx=m.m[0] * v.v[0] + m.m[4] * v.v[1] + m.m[8] * v.v[2] + m.m[12];
	glmath_real ty=m.m[1] * v.v[0] + m.m[5] * v.v[1] + m.m[9] * v.v[2] + m.m[13];
	glmath_real tz=m.m[2] * v.v[0] + m.m[6] * v.v[1] + m.m[10] * v.v[2] + m.m[14];
	glmath_mat4 mr={{
		m.m[0], m.m[1], m.m[2], m.m[3],
		m.m[4], m.m[5], m.m[6], m.m[7],
		m.m[8], m.m[9], m.m[10], m.m[11],
		tx, ty, tz, m.m[15]
	}};
	return mr;
}

static inline glmath_mat4 glmath_mat4_translate_vec4(glmath_mat4 m, glmath_vec4 v){
	glmath_real tx=m.m[0] * v.v[0] + m.m[4] * v.v[1] + m.m[8] * v.v[2] + m.m[12];
	glmath_real ty=m.m[1] * v.v[0] + m.m[5] * v.v[1] + m.m[9] * v.v[2] + m.m[13];
	glmath_real tz=m.m[2] * v.v[0] + m.m[6] * v.v[1] + m.m[10] * v.v[2] + m.m[14];
	glmath_mat4 mr={{
		m.m[0], m.m[1], m.m[2], m.m[3],
		m.m[4], m.m[5], m.m[6], m.m[7],
		m.m[8], m.m[9], m.m[10], m.m[11],
		tx, ty, tz, m.m[15]
	}};
	return mr;
}

static inline glmath_mat4 glmath_mat4_create_scale(glmath_real x, glmath_real y, glmath_real z){
	glmath_mat4 m={{
		x, 0, 0, 0,
		0, y, 0, 0,
		0, 0, z, 0,
		0, 0, 0, 1.0f
	}};
	return m;
}

static inline glmath_mat4 glmath_mat4_create_scale_vec3(glmath_vec3 v){
	glmath_mat4 m={{
		v.v[0], 0, 0, 0,
		0, v.v[1], 0, 0,
		0, 0, v.v[2], 0,
		0, 0, 0, 1.0f
	}};
	return m;
}

static inline glmath_mat4 glmath_mat4_create_scale_vec4(glmath_vec4 v){
	glmath_mat4 m={{
		v.v[0], 0, 0, 0,
		0, v.v[1], 0, 0,
		0, 0, v.v[2], 0,
		0, 0, 0, 1.0f
	}};
	return m;
}

static inline glmath_mat4 glmath_mat4_scale(glmath_mat4 m, glmath_real x, glmath_real y, glmath_real z){
	glmath_mat4 mr={{
		x*m.m[0], x*m.m[1], x*m.m[2], x*m.m[3],
		y*m.m[4], y*m.m[5], y*m.m[6], y*m.m[7],
		z*m.m[8], z*m.m[9], z*m.m[10], z*m.m[11],
		m.m[12], m.m[13], m.m[14], m.m[15]
	}};
	return mr;
}

static inline glmath_mat4 glmath_mat4_scale_vec3(glmath_mat4 m, glmath_vec3 v){
	glmath_mat4 mr={{
		v.v[0]*m.m[0], v.v[0]*m.m[1], v.v[0]*m.m[2], v.v[0]*m.m[3],
		v.v[1]*m.m[4], v.v[1]*m.m[5], v.v[1]*m.m[6], v.v[1]*m.m[7],
		v.v[2]*m.m[8], v.v[2]*m.m[9], v.v[2]*m.m[10], v.v[2]*m.m[11],
		m.m[12], m.m[13], m.m[14], m.m[15]
	}};
	return mr;
}

static inline glmath_mat4 glmath_mat4_scale_vec4(glmath_mat4 m, glmath_vec4 v){
	glmath_mat4 mr={{
		v.v[0]*m.m[0], v.v[0]*m.m[1], v.v[0]*m.m[2], v.v[0]*m.m[3],
		v.v[1]*m.m[4], v.v[1]*m.m[5], v.v[1]*m.m[6], v.v[1]*m.m[7],
		v.v[2]*m.m[8], v.v[2]*m.m[9], v.v[2]*m.m[10], v.v[2]*m.m[11],
		m.m[12], m.m[13], m.m[14], m.m[15]
	}};
	return mr;
}

static inline glmath_mat4 glmath_mat4_create_rotation(glmath_real rad, glmath_vec3 axis){
	glmath_real sin=glmath_sin(rad);
	glmath_real cos=glmath_cos(rad);
	glmath_real cos2=1-cos;
	axis=glmath_vec3_norm(axis);
	
	glmath_mat4 m={{
		axis.v[0]*axis.v[0]*cos2 + cos, axis.v[1]*axis.v[0]*cos2 - axis.v[2]*sin, axis.v[2]*axis.v[0]*cos2 + axis.v[1]*sin, 0.0f,
		axis.v[0]*axis.v[1]*cos2 + axis.v[2]*sin, axis.v[1]*axis.v[1]*cos2 + cos, axis.v[2]*axis.v[1]*cos2 - axis.v[0]*sin, 0.0f,
		axis.v[0]*axis.v[2]*cos2 - axis.v[1]*sin, axis.v[1]*axis.v[2]*cos2 + axis.v[0]*sin, axis.v[2]*axis.v[2]*cos2 + cos, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	}};
	return m;
}


static inline glmath_mat4 glmath_mat4_create_xrotation(glmath_real rad){
	glmath_real sin=glmath_sin(rad);
	glmath_real cos=glmath_cos(rad);
	glmath_mat4 m={{
		1, 0,   0,    0,
		0, cos, sin,  0,
		0, -sin, cos, 0,
		0, 0, 	0, 	  1
	}};
	return m;
}

static inline glmath_mat4 glmath_mat4_create_yrotation(glmath_real rad){
	glmath_real sin=glmath_sin(rad);
	glmath_real cos=glmath_cos(rad);
	glmath_mat4 m={{
		cos, 0, -sin, 0,
		0, 	 1, 0,    0,
		sin, 0, cos,  0,
		0, 	 0, 0,	  1
	}};
	return m;
}

static inline glmath_mat4 glmath_mat4_create_zrotation(glmath_real rad){
	glmath_real sin=glmath_sin(rad);
	glmath_real cos=glmath_cos(rad);
	glmath_mat4 m={{
		cos, sin,   0, 0,
		-sin, cos,  0, 0,
		0,    0,    1, 0,
		0, 	  0,    0, 1
	}};
	return m;
}

static inline glmath_mat4 glmath_mat4_create_rotation_euler_angles(glmath_euler_angles a){
	glmath_mat4 mr=glmath_mat4_create_identity();
	mr=glmath_mat4_mul(mr, glmath_mat4_create_xrotation(a.r[0]));
	mr=glmath_mat4_mul(mr, glmath_mat4_create_yrotation(a.r[1]));
	mr=glmath_mat4_mul(mr, glmath_mat4_create_zrotation(a.r[2]));
	return mr;
}

static inline glmath_mat4 glmath_mat4_rotate(glmath_mat4 m, glmath_real rad, glmath_vec3 axis){
	glmath_mat4 mr=glmath_mat4_create_rotation(rad, axis);
	mr=glmath_mat4_mul(m, mr);
	return mr;
}

static inline glmath_mat4 glmath_mat4_xrotate(glmath_mat4 m, glmath_real rad){
	glmath_mat4 mr=glmath_mat4_create_xrotation(rad);
	mr=glmath_mat4_mul(m, mr);
	return mr;
}

static inline glmath_mat4 glmath_mat4_yrotate(glmath_mat4 m, glmath_real rad){
	glmath_mat4 mr=glmath_mat4_create_yrotation(rad);
	mr=glmath_mat4_mul(m, mr);
	return mr;
}

static inline glmath_mat4 glmath_mat4_zrotate(glmath_mat4 m, glmath_real rad){
	glmath_mat4 mr=glmath_mat4_create_zrotation(rad);
	mr=glmath_mat4_mul(m, mr);
	return mr;
}

static inline glmath_mat4 glmath_mat4_rotate_euler_angles(glmath_mat4 m, glmath_euler_angles a){
	glmath_mat4 mr=glmath_mat4_create_identity();
	mr=glmath_mat4_mul(mr, glmath_mat4_create_xrotation(a.r[0]));
	mr=glmath_mat4_mul(mr, glmath_mat4_create_yrotation(a.r[1]));
	mr=glmath_mat4_mul(mr, glmath_mat4_create_zrotation(a.r[2]));
	return glmath_mat4_mul(m, mr);
}

static inline glmath_mat4 glmath_mat4_create_perspective(glmath_real fov, glmath_real aspect, glmath_real near, glmath_real far){
	glmath_real tanHFov=glmath_tan(fov/2);
	glmath_real x_scale = 1/(tanHFov*aspect);
	glmath_real y_scale = 1/tanHFov;
	glmath_real frustum_length = far-near;
	
	glmath_mat4 m={{
		x_scale, 0, 	  0, 							0,
		0, 		 y_scale, 0, 							0,
		0,		 0,		  -(far + near)/frustum_length, -1.0f,
		0,		 0,		  -(2*near*far)/frustum_length, 0,
	}};
	return m;
}


static inline glmath_mat4 glmath_mat4_create_frustum(glmath_real near, glmath_real far){
	glmath_real frustum_length = far-near;
	
	glmath_mat4 m={{
		1.0f,	 0, 	  0, 							0,
		0, 		 1.0f,	  0, 							0,
		0,		 0,		  -(far + near)/frustum_length, -1.0f,
		0,		 0,		  -(2*near*far)/frustum_length, 0,
	}};
	return m;
}

static inline glmath_mat4 glmath_mat4_create_ortho(glmath_real left, glmath_real right, glmath_real top, glmath_real bottom, glmath_real near, glmath_real far){
	glmath_real rl = right-left;
	glmath_real tb = top-bottom;
	glmath_real fn = far-near;
	
	glmath_mat4 m={{
		2/rl,	 		  0, 	  			0, 			   0,
		0, 		 		  2/tb,	  			0, 			   0,
		0,		 		  0,		  		-2/fn,		   0,
		-(right+left)/rl, -(top+left)/tb,	-(far+near)/fn, 1.0f
	}};
	return m;
}

static inline glmath_mat4 glmath_mat4_create_lookat_vec3(glmath_vec3 eye, glmath_vec3 center, glmath_vec3 up){
	
	glmath_vec3 f=glmath_vec3_norm(glmath_vec3_sub(center, eye));
	glmath_vec3 s=glmath_vec3_norm(glmath_vec3_cross(f, up));
	glmath_vec3 u=glmath_vec3_cross(s,f);
	glmath_mat4 mr={{
		s.v[0], u.v[0], -f.v[0], 0,
		s.v[1], u.v[1], -f.v[1], 0,
		s.v[2], u.v[2], -f.v[2], 0,
		-glmath_vec3_dot(s, eye), -glmath_vec3_dot(u, eye), glmath_vec3_dot(f, eye), 1.0f
	}};
	return mr;
}


static inline glmath_mat4 glmath_mat4_create_lookat(glmath_real eyeX, glmath_real eyeY, glmath_real eyeZ, glmath_real centerX, glmath_real centerY, glmath_real centerZ, glmath_real upX, glmath_real upY, glmath_real upZ){
	glmath_vec3 eye=glmath_vec3_create(eyeX, eyeY, eyeZ);
	glmath_vec3 center=glmath_vec3_create(centerX, centerY, centerZ);
	glmath_vec3 up=glmath_vec3_norm(glmath_vec3_create(upX, upY, upZ));
	
	return glmath_mat4_create_lookat_vec3(eye, center, up);
}

static inline glmath_mat4 glmath_mat4_create_view(glmath_vec3 p, glmath_vec3 f, glmath_vec3 u){
	glmath_vec3 fr=glmath_vec3_norm(f);
	glmath_vec3 up=glmath_vec3_norm(u);
	glmath_vec3 r=glmath_vec3_norm(glmath_vec3_cross(fr, up));
	up=glmath_vec3_cross(r, fr);
	glmath_mat4 mr={{
		r.v[0], up.v[0], -fr.v[0], 0,
		r.v[1], up.v[1], -fr.v[1], 0,
		r.v[2], up.v[2], -fr.v[2], 0,
		-glmath_vec3_dot(r, p), -glmath_vec3_dot(up, p), glmath_vec3_dot(fr, p), 1.0f
	}};
	return mr;
	
}

static inline glmath_transform_params glmath_mat4_decompose(glmath_mat4 m){
	glmath_transform_params p={0,0,0,0,0,0,0,0,0};
	p.t.x=m.m[12];
	p.t.y=m.m[13];
	p.t.z=m.m[14];
	
	m.m[12]=0;
	m.m[13]=0;
	m.m[14]=0;
	
	p.s.x=glmath_vec3_mag(glmath_vec3_create(m.m[0], m.m[1], m.m[2]));
	p.s.y=glmath_vec3_mag(glmath_vec3_create(m.m[4], m.m[5], m.m[6]));
	p.s.z=glmath_vec3_mag(glmath_vec3_create(m.m[8], m.m[9], m.m[10]));
	
	m.m[0]/=p.s.x;
	m.m[5]/=p.s.y;
	m.m[10]/=p.s.z;
	
	p.r.x=glmath_atan2(m.m[6], m.m[10]);
	p.r.y=glmath_atan2(-m.m[2], glmath_sqrt(m.m[0]*m.m[0]+m.m[1]*m.m[1]));
	glmath_real s=glmath_sin(p.r.x);
	glmath_real c=glmath_cos(p.r.x);
	p.r.z=glmath_atan2(s*m.m[8]-c*m.m[4], c*m.m[5]-s*m.m[9]);
	
	return p;
}

static inline glmath_vec4 glmath_mat4_mul_vec4(glmath_mat4 m, glmath_vec4 v){
	glmath_vec4 vr={{
		m.m[0]*v.v[0] + m.m[4]*v.v[1] + m.m[8]*v.v[2] +  m.m[12]*v.v[3],
		m.m[1]*v.v[0] + m.m[5]*v.v[1] + m.m[9]*v.v[2] +  m.m[13]*v.v[3],
		m.m[2]*v.v[0] + m.m[6]*v.v[1] + m.m[10]*v.v[2] + m.m[14]*v.v[3],
		m.m[3]*v.v[0] + m.m[7]*v.v[1] + m.m[11]*v.v[2] + m.m[15]*v.v[3]
	}};
	return vr;
}

static inline glmath_mat4 glmath_mat4_add(glmath_mat4 m1, glmath_mat4 m2){
	glmath_mat4 m;
#if defined(GLMATH_SINGLE_PRECISION) && defined(GLMATH_AVX)
	_mm256_store_ps(&m.m[0], _mm256_load_ps(&m1.m[0]) + _mm256_load_ps(&m2.m[0]));
	_mm256_store_ps(&m.m[8], _mm256_load_ps(&m1.m[8]) + _mm256_load_ps(&m2.m[8]));
#elif defined(GLMATH_SINGLE_PRECISION) && defined(GLMATH_SSE)
	_mm_store_ps(&m.m[0], _mm_load_ps(&m1.m[0]) + _mm_load_ps(&m2.m[0]));
	_mm_store_ps(&m.m[4], _mm_load_ps(&m1.m[4]) + _mm_load_ps(&m2.m[4]));
	_mm_store_ps(&m.m[8], _mm_load_ps(&m1.m[8]) + _mm_load_ps(&m2.m[8]));
	_mm_store_ps(&m.m[12], _mm_load_ps(&m1.m[12]) + _mm_load_ps(&m2.m[12]));
#elif defined(GLMATH_DOUBLE_PRECISION) && defined(GLMATH_AVX)
	_mm256_store_pd(&m.m[0], _mm256_load_pd(&m1.m[0]) + _mm256_load_pd(&m2.m[0]));
	_mm256_store_pd(&m.m[4], _mm256_load_pd(&m1.m[4]) + _mm256_load_pd(&m2.m[4]));
	_mm256_store_pd(&m.m[8], _mm256_load_pd(&m1.m[8]) + _mm256_load_pd(&m2.m[8]));
	_mm256_store_pd(&m.m[12], _mm256_load_pd(&m1.m[12]) + _mm256_load_pd(&m2.m[12]));
#elif defined(GLMATH_DOUBLE_PRECISION) && defined(GLMATH_SSE)
	_mm_store_pd(&m.m[0], _mm_load_pd(&m1.m[0]) + _mm_load_pd(&m2.m[0]));
	_mm_store_pd(&m.m[2], _mm_load_pd(&m1.m[2]) + _mm_load_pd(&m2.m[2]));
	_mm_store_pd(&m.m[4], _mm_load_pd(&m1.m[4]) + _mm_load_pd(&m2.m[4]));
	_mm_store_pd(&m.m[6], _mm_load_pd(&m1.m[6]) + _mm_load_pd(&m2.m[6]));
	_mm_store_pd(&m.m[8], _mm_load_pd(&m1.m[8]) + _mm_load_pd(&m2.m[8]));
	_mm_store_pd(&m.m[10], _mm_load_pd(&m1.m[10]) + _mm_load_pd(&m2.m[10]));
	_mm_store_pd(&m.m[12], _mm_load_pd(&m1.m[12]) + _mm_load_pd(&m2.m[12]));
	_mm_store_pd(&m.m[14], _mm_load_pd(&m1.m[14]) + _mm_load_pd(&m2.m[14]));
	
#else
	m.m[0]=m1.m[0]+m2.m[0];
	m.m[1]=m1.m[1]+m2.m[1];
	m.m[2]=m1.m[2]+m2.m[2];
	m.m[3]=m1.m[3]+m2.m[3];
	m.m[4]=m1.m[4]+m2.m[4];
	m.m[5]=m1.m[5]+m2.m[5];
	m.m[6]=m1.m[6]+m2.m[6];
	m.m[7]=m1.m[7]+m2.m[7];
	m.m[8]=m1.m[8]+m2.m[8];
	m.m[9]=m1.m[9]+m2.m[9];
	m.m[10]=m1.m[10]+m2.m[10];
	m.m[11]=m1.m[11]+m2.m[11];
	m.m[12]=m1.m[12]+m2.m[12];
	m.m[13]=m1.m[13]+m2.m[13];
	m.m[14]=m1.m[14]+m2.m[14];
	m.m[15]=m1.m[15]+m2.m[15];
#endif
	return m;
}


static inline glmath_mat4 glmath_mat4_sub(glmath_mat4 m1, glmath_mat4 m2){
	glmath_mat4 m;
#ifdef GLMATH_SSE3_INS
#ifdef GLMATH_SINGLE_PRECISION
	_mm_store_ps(&m.m[0], _mm_load_ps(&m1.m[0]) - _mm_load_ps(&m2.m[0]));
	_mm_store_ps(&m.m[4], _mm_load_ps(&m1.m[4]) - _mm_load_ps(&m2.m[4]));
	_mm_store_ps(&m.m[8], _mm_load_ps(&m1.m[8]) - _mm_load_ps(&m2.m[8]));
	_mm_store_ps(&m.m[12], _mm_load_ps(&m1.m[12]) - _mm_load_ps(&m2.m[12]));
#endif
#ifdef GLMATH_DOUBLE_PRECISION
	_mm_store_pd(&m.m[0], _mm_load_pd(&m1.m[0]) - _mm_load_pd(&m2.m[0]));
	_mm_store_pd(&m.m[2], _mm_load_pd(&m1.m[2]) - _mm_load_pd(&m2.m[2]));
	_mm_store_pd(&m.m[4], _mm_load_pd(&m1.m[4]) - _mm_load_pd(&m2.m[4]));
	_mm_store_pd(&m.m[6], _mm_load_pd(&m1.m[6]) - _mm_load_pd(&m2.m[6]));
	_mm_store_pd(&m.m[8], _mm_load_pd(&m1.m[8]) - _mm_load_pd(&m2.m[8]));
	_mm_store_pd(&m.m[10], _mm_load_pd(&m1.m[10]) - _mm_load_pd(&m2.m[10]));
	_mm_store_pd(&m.m[12], _mm_load_pd(&m1.m[12]) - _mm_load_pd(&m2.m[12]));
	_mm_store_pd(&m.m[14], _mm_load_pd(&m1.m[14]) - _mm_load_pd(&m2.m[14]));
#endif
#else
	m.m[0]= m1.m[0]-m2.m[0];
	m.m[1]= m1.m[1]-m2.m[1];
	m.m[2]= m1.m[2]-m2.m[2];
	m.m[3]= m1.m[3]-m2.m[3];
	m.m[4]= m1.m[4]-m2.m[4];
	m.m[5]= m1.m[5]-m2.m[5];
	m.m[6]= m1.m[6]-m2.m[6];
	m.m[7]= m1.m[7]-m2.m[7];
	m.m[8]= m1.m[8]-m2.m[8];
	m.m[9]= m1.m[9]-m2.m[9];
	m.m[10]= m1.m[10]-m2.m[10];
	m.m[11]= m1.m[11]-m2.m[11];
	m.m[12]= m1.m[12]-m2.m[12];
	m.m[13]= m1.m[13]-m2.m[13];
	m.m[14]= m1.m[14]-m2.m[14];
	m.m[15]= m1.m[15]-m2.m[15];
#endif
	return m;
}

static inline glmath_mat4 glmath_mat4_mul(glmath_mat4 m1, glmath_mat4 m2){
	glmath_mat4 m;
//#if defined(GLMATH_SINGLE_PRECISION) && defined(GLMATH_SSE3)
//	const __m128 _r0=_mm_load_ps(&m2.m[0]);
//	const __m128 _r1=_mm_load_ps(&m2.m[4]);
//	const __m128 _r2=_mm_load_ps(&m2.m[8]);
//	const __m128 _r3=_mm_load_ps(&m2.m[12]);
//	const __m128 _l0=_mm_load_ps(&m1.m[0]);
//	const __m128 _l1=_mm_load_ps(&m1.m[4]);
//	const __m128 _l2=_mm_load_ps(&m1.m[8]);
//	const __m128 _l3=_mm_load_ps(&m1.m[12]);
//	
//	const __m128 ma= _l0 * _mm_shuffle_ps(_r0, _r0, _MM_SHUFFLE(0,0,0,0))
//	+  _l1 * _mm_shuffle_ps(_r0, _r0, _MM_SHUFFLE(1,1,1,1))
//	+  _l2 * _mm_shuffle_ps(_r0, _r0, _MM_SHUFFLE(2,2,2,2))
//	+  _l3 * _mm_shuffle_ps(_r0, _r0, _MM_SHUFFLE(3,3,3,3));
//	
//	const __m128 mb= _l0 * _mm_shuffle_ps(_r1, _r1, _MM_SHUFFLE(0,0,0,0))
//	+  _l1 * _mm_shuffle_ps(_r1, _r1, _MM_SHUFFLE(1,1,1,1))
//	+  _l2 * _mm_shuffle_ps(_r1, _r1, _MM_SHUFFLE(2,2,2,2))
//	+  _l3 * _mm_shuffle_ps(_r1, _r1, _MM_SHUFFLE(3,3,3,3));
//	
//	const __m128 mc= _l0 * _mm_shuffle_ps(_r2, _r2, _MM_SHUFFLE(0,0,0,0))
//	+  _l1 * _mm_shuffle_ps(_r2, _r2, _MM_SHUFFLE(1,1,1,1))
//	+  _l2 * _mm_shuffle_ps(_r2, _r2, _MM_SHUFFLE(2,2,2,2))
//	+  _l3 * _mm_shuffle_ps(_r2, _r2, _MM_SHUFFLE(3,3,3,3));
//	
//	const __m128 md= _l0 * _mm_shuffle_ps(_r3, _r3, _MM_SHUFFLE(0,0,0,0))
//	+  _l1 * _mm_shuffle_ps(_r3, _r3, _MM_SHUFFLE(1,1,1,1))
//	+  _l2 * _mm_shuffle_ps(_r3, _r3, _MM_SHUFFLE(2,2,2,2))
//	+  _l3 * _mm_shuffle_ps(_r3, _r3, _MM_SHUFFLE(3,3,3,3));
//	
//	_mm_store_ps(&m.m[0], ma);
//	_mm_store_ps(&m.m[4], mb);
//	_mm_store_ps(&m.m[8], mc);
//	_mm_store_ps(&m.m[12], md);
//#else
	m.m[0]= m2.m[0]*m1.m[0] + m2.m[1]*m1.m[4] + m2.m[2]*m1.m[8] + m2.m[3]*m1.m[12];
	m.m[1]= m2.m[0]*m1.m[1] + m2.m[1]*m1.m[5] + m2.m[2]*m1.m[9] + m2.m[3]*m1.m[13];
	m.m[2]= m2.m[0]*m1.m[2] + m2.m[1]*m1.m[6] + m2.m[2]*m1.m[10] + m2.m[3]*m1.m[14];
	m.m[3]= m2.m[0]*m1.m[3] + m2.m[1]*m1.m[7] + m2.m[2]*m1.m[11] + m2.m[3]*m1.m[15];
	
	m.m[4]= m2.m[4]*m1.m[0] + m2.m[5]*m1.m[4] + m2.m[6]*m1.m[8] + m2.m[7]*m1.m[12];
	m.m[5]= m2.m[4]*m1.m[1] + m2.m[5]*m1.m[5] + m2.m[6]*m1.m[9] + m2.m[7]*m1.m[13];
	m.m[6]= m2.m[4]*m1.m[2] + m2.m[5]*m1.m[6] + m2.m[6]*m1.m[10] + m2.m[7]*m1.m[14];
	m.m[7]= m2.m[4]*m1.m[3] + m2.m[5]*m1.m[7] + m2.m[6]*m1.m[11] + m2.m[7]*m1.m[15];
	
	m.m[8]= m2.m[8]*m1.m[0] + m2.m[9]*m1.m[4] + m2.m[10]*m1.m[8] + m2.m[11]*m1.m[12];
	m.m[9]= m2.m[8]*m1.m[1] + m2.m[9]*m1.m[5] + m2.m[10]*m1.m[9] + m2.m[11]*m1.m[13];
	m.m[10]= m2.m[8]*m1.m[2] + m2.m[9]*m1.m[6] + m2.m[10]*m1.m[10] + m2.m[11]*m1.m[14];
	m.m[11]= m2.m[8]*m1.m[3] + m2.m[9]*m1.m[7] + m2.m[10]*m1.m[11] + m2.m[11]*m1.m[15];
	
	m.m[12]= m2.m[12]*m1.m[0] + m2.m[13]*m1.m[4] + m2.m[14]*m1.m[8] + m2.m[15]*m1.m[12];
	m.m[13]= m2.m[12]*m1.m[1] + m2.m[13]*m1.m[5] + m2.m[14]*m1.m[9] + m2.m[15]*m1.m[13];
	m.m[14]= m2.m[12]*m1.m[2] + m2.m[13]*m1.m[6] + m2.m[14]*m1.m[10] + m2.m[15]*m1.m[14];
	m.m[15]= m2.m[12]*m1.m[3] + m2.m[13]*m1.m[7] + m2.m[14]*m1.m[11] + m2.m[15]*m1.m[15];
//#endif
	return m;
}