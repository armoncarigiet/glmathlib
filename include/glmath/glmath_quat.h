//===========================================================================
/*
 *  glmath - OpenGL Mathematics Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file glmath_quat.h
 *	@date 23.01.15.
 *  @author Armon Carigiet
 *
 *  @brief Provides algebra and transformation functions for quaternions (glmath_quat).
 *  @see glmath_types.h
 */
//===========================================================================

#ifndef glmath_glmath_quat_h
#define glmath_glmath_quat_h

#include "glmath_types.h"
#include "glmath_vec4.h"

#ifdef __cplusplus
extern "C" {
#endif
	
//! Builds an identity quaternion.
//! @return identity quaternion
static inline glmath_quat glmath_quat_create_identity();
	
//! Builds a quaternion from components.
//! @param w w quaternion component
//! @param x x quaternion component
//! @param y y quaternion component
//! @param z z quaternion component
//! @return quaternion
static inline glmath_quat glmath_quat_create(glmath_real w, glmath_real x, glmath_real y, glmath_real z);
	
//! Builds a quaternion from an vector(3) and a scalar value.
//! @param w real scalar value
//! @param v imaginary values
//! @return quaternion
static inline glmath_quat glmath_quat_create_vec3(glmath_real w, glmath_vec3 v);
	
//! Builds a quaternion from an 3x3 matrix.
//! @param m matrix
//! @return quaternion
static inline glmath_quat glmath_quat_create_mat3(glmath_mat3 m);
	
//! Builds a quaternion from an 4x4 matrix.
//! @param m matrix
//! @return quaternion
static inline glmath_quat glmath_quat_create_mat4(glmath_mat4 m);
	
//! Builds a rotation quaternion from an angle and an axis vector(3).
//! @note Rotation angle is expressed in radiants!
//! @note axis vector should be normalized!
//! @param rad rotation angle
//! @param v rotation axis
//! @return rotation quaternion
static inline glmath_quat glmath_quat_create_rotation(glmath_real rad, glmath_vec3 v);
	
//! Builds a rotation quaternion from an angle and the x-axis as axis vector(3).
//! @note Rotation angle is expressed in radiants!
//! @param rad rotation angle
//! @return rotation quaternion
static inline glmath_quat glmath_quat_create_xrotation(glmath_real rad);
	
//! Builds a rotation quaternion from an angle and the y-axis as axis vector(3).
//! @note Rotation angle is expressed in radiants!
//! @param rad rotation angle
//! @return rotation quaternion
static inline glmath_quat glmath_quat_create_yrotation(glmath_real rad);
	
//! Builds a rotation quaternion from an angle and the z-axis as axis vector(3).
//! @note Rotation angle is expressed in radiants!
//! @param rad rotation angle
//! @return rotation quaternion
static inline glmath_quat glmath_quat_create_zrotation(glmath_real rad);
	
//! Builds a rotation quaternion from euler angles.
//! @param rad rotation angle
//! @return rotation quaternion
static inline glmath_quat glmath_quat_create_rotation_euler_angles(glmath_euler_angles a);
	
//! Rotates a vector(3) by a quaternion.
//! @param v vector
//! @param q rotation quaternion
//! @return rotated vector
static inline glmath_vec3 glmath_quat_rotate_vec3(glmath_vec3 v, glmath_quat q);
	
//! Rotates a vector(4) by a quaternion.
//! @note The w component of the vector is ignored in this transformation.
//! @param v vector
//! @param q rotation quaternion
//! @return rotated vector
static inline glmath_vec4 glmath_quat_rotate_vec4(glmath_vec4 v, glmath_quat q);
	
//! Calculates the rotation angle of a quaternion.
//! @param q quaternion
//! @return rotation angle
static inline glmath_real glmath_quat_angle(glmath_quat q);
	
//! Calculates the rotation axis of a quaternion.
//! @param q quaternion
//! @return rotation axis
static inline glmath_vec3 glmath_quat_axis(glmath_quat q);
	
//! Calculates the magnitude of a quaternion.
//! @param q quaternion
//! @return magnitude
static inline glmath_real glmath_quat_mag(glmath_quat q);
	
//! Adds two quaternion.
//! @param q1 quaternion
//! @param q2 quaternion
//! @return resulting quaternion
static inline glmath_quat glmath_quat_add(glmath_quat q1, glmath_quat q2);
	
//! Subtracts two quaternion.
//! @param q1 quaternion
//! @param q2 quaternion
//! @return resulting quaternion
static inline glmath_quat glmath_quat_sub(glmath_quat q1, glmath_quat q2);
	
//! Multiplication two quaternion.
//! @param q1 quaternion
//! @param q2 quaternion
//! @return resulting quaternion
static inline glmath_quat glmath_quat_mul(glmath_quat q1, glmath_quat q2);
	
//! Calculates the dot-product of two quaternions.
//! @param v1 first quaternion
//! @param v2 second quaternion
//! @return dot product
static inline glmath_real glmath_quat_dot(glmath_quat q1, glmath_quat q2);
	
//! Calculates the cross-product of two quaternions.
//! @param v1 left quaternion
//! @param v2 right quaternion
//! @return resulting quaternion
static inline glmath_quat glmath_quat_cross(glmath_quat q1, glmath_quat q2);
	
//! Conjugates a quaternion.
//! @param q quaternion
//! @return conjugated quaternion
static inline glmath_quat glmath_quat_conj(glmath_quat q);
	
//! Inverts a quaternion.
//! @param q quaternion
//! @return inverted quaternion
static inline glmath_quat glmath_quat_invert(glmath_quat q);
	
//! Normalizes a quaternion.
//! @param q quaternion
//! @return normalizes quaternion
static inline glmath_quat glmath_quat_norm(glmath_quat q);
	
//! Interpolates spherical between two quaternions.
//! @param q0 start quaternion
//! @param q1 end quaternion
//! @param t interpolation scalar
//! @return interpolated quaternion
static inline glmath_quat glmath_quat_slerp(glmath_quat q0, glmath_quat q1, glmath_real t);
	
#include "glmath_quat.inl"
	
#ifdef __cplusplus
}
#endif

#endif