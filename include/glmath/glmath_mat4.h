//===========================================================================
/*
 *  glmath - OpenGL Mathematics Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file glmath_mat4.h
 *	@date 18.01.15.
 *  @author Armon Carigiet
 *
 *  @brief Provides algebra and transformation functions for 4x4 matrices (glmath_mat4).
 *  @see glmath_types.h
 */
//===========================================================================

#ifndef glmath_glmath_mat4_h
#define glmath_glmath_mat4_h

#include "glmath_types.h"
#include "glmath_mat3.h"
#include "glmath_vec4.h"
#include "glmath_vec3.h"
#include "glmath_quat.h"

#ifdef __cplusplus
extern "C" {
#endif
	
//! Builds a 4x4 identity matrix.
//! @return identity matrix
static inline glmath_mat4 glmath_mat4_create_identity();
	
//! Builds a 4x4 matrix from matrix components.
//! @param m00-m33 matrix components
//! @return matrix
static inline glmath_mat4 glmath_mat4_create(glmath_real m00, glmath_real m01, glmath_real m02, glmath_real m03, glmath_real m10, glmath_real m11, glmath_real m12, glmath_real m13, glmath_real m20, glmath_real m21, glmath_real m22, glmath_real m23, glmath_real m30, glmath_real m31, glmath_real m32, glmath_real m33);
	
//! Builds a 4x4 matrix from a quaternion.
//! @param q quaternion
//! @return matrix
static inline glmath_mat4 glmath_mat4_create_quat(glmath_quat q);
	
//! Builds a 3x3 matrix from a 4x4 matrix
//! @param m matrix
//! @return extracted matrix
static inline glmath_mat3 glmath_mat4_get_mat3(glmath_mat4 m);
	
//! Transposes a 4x4 matrix.
//! @param m matrix
//! @return transposed matrix
static inline glmath_mat4 glmath_mat4_transpose(glmath_mat4 m);
	
//! Calculates the determinant of a 4x4 matrix.
//! @param m matrix
//! @return determinant
static inline glmath_real glmath_mat4_det(glmath_mat4 m);
	
//! Inverts a 4x4 matrix.
//! @param m matrix
//! @param invertable is set to true if m is invertable, else to false.
//! @return inverted matrix
static inline glmath_mat4 glmath_mat4_invert(glmath_mat4 m, bool* invertable);
	
//! Builds 4x4 translation matrix from components.
//! @param x x translation component
//! @param y y translation component
//! @param z z translation component
//! @return translation matrix
static inline glmath_mat4 glmath_mat4_create_translation(glmath_real x, glmath_real y, glmath_real z);
	
//! Builds 4x4 translation matrix from a vector(3).
//! @param v translation vector
//! @return translation matrix
static inline glmath_mat4 glmath_mat4_create_translation_vec3(glmath_vec3 v);
	
//! Builds 4x4 translation matrix from a vector(4).
//! @param v translation vector
//! @return translation matrix
static inline glmath_mat4 glmath_mat4_create_translation_vec4(glmath_vec4 v);
	
//! Multiplicates a 4x4 matrix by a 4x4 translation matrix created from components.
//! @param m input matrix
//! @param x x translation component
//! @param y y translation component
//! @param z z translation component
//! @return translated matrix
static inline glmath_mat4 glmath_mat4_translate(glmath_mat4 m, glmath_real x, glmath_real y, glmath_real z);
	
//! Multiplicates a 4x4 matrix by a 4x4 translation matrix created from a vector(3).
//! @param m input matrix
//! @param v translation vector
//! @return translated matrix
static inline glmath_mat4 glmath_mat4_translate_vec3(glmath_mat4 m, glmath_vec3 v);
	
//! Multiplicates a 4x4 matrix by a 4x4 translation matrix created from a vector(4).
//! @param m input matrix
//! @param v translation vector
//! @return translated matrix
static inline glmath_mat4 glmath_mat4_translate_vec4(glmath_mat4 m, glmath_vec4 v);
	
//! Builds a 4x4 scale matrix from components.
//! @param x x scale component
//! @param y y scale component
//! @param z z scale component
//! @return scale matrix
static inline glmath_mat4 glmath_mat4_create_scale(glmath_real x, glmath_real y, glmath_real z);
	
//! Builds a 4x4 scale matrix from a vector(3).
//! @param v scale vector
//! @return scale matrix
static inline glmath_mat4 glmath_mat4_create_scale_vec3(glmath_vec3 v);
	
//! Builds a 4x4 scale matrix from a vector(4).
//! @param v scale vector
//! @return scale matrix
static inline glmath_mat4 glmath_mat4_create_scale_vec4(glmath_vec4 v);
	
//! Scales a 4x4 matrix by components.
//! @param m input matrix
//! @param x x scale component
//! @param y y scale component
//! @param z z scale component
//! @return scaled matrix
static inline glmath_mat4 glmath_mat4_scale(glmath_mat4 m, glmath_real x, glmath_real y, glmath_real z);
	
//! Scales a 4x4 matrix by an vector(3).
//! @param m input matrix
//! @param v scale vector
//! @return scaled matrix
static inline glmath_mat4 glmath_mat4_scale_vec3(glmath_mat4 m, glmath_vec3 v);
	
//! Scales a 4x4 matrix by an vector(4).
//! @param m input matrix
//! @param v scale vector
//! @return scaled matrix
static inline glmath_mat4 glmath_mat4_scale_vec4(glmath_mat4 m, glmath_vec4 v);
	
//! Builds a 4x4 rotation matrix from an angle and an axis vector(3).
//! @note Rotation angle is expressed in radiants!
//! @note axis vector should be normalized!
//! @param rad rotation angle
//! @param axis rotation axis
//! @return rotation matrix
static inline glmath_mat4 glmath_mat4_create_rotation(glmath_real rad, glmath_vec3 axis);
	
//! Builds a 4x4 rotation matrix from an angle and the x-axis as axis vector(3).
//! @note Rotation angle is expressed in radiants!
//! @param rad rotation angle
//! @return rotation matrix
static inline glmath_mat4 glmath_mat4_create_xrotation(glmath_real rad);
	
//! Builds a 4x4 rotation matrix from an angle and the y-axis as axis vector(3).
//! @note Rotation angle is expressed in radiants!
//! @param rad rotation angle
//! @return rotation matrix
static inline glmath_mat4 glmath_mat4_create_yrotation(glmath_real rad);
	
//! Builds a 4x4 rotation matrix from an angle and the z-axis as axis vector(3).
//! @note Rotation angle is expressed in radiants!
//! @param rad rotation angle
//! @return rotation matrix
static inline glmath_mat4 glmath_mat4_create_zrotation(glmath_real rad);
	
//! Builds a 4x4 rotation matrix from euler angles.
//! @param a rotation angles
//! @return rotation matrix
static inline glmath_mat4 glmath_mat4_create_rotation_euler_angles(glmath_euler_angles a);
	
//! Multiplicates a 4x4 matrix by a 4x4 rotation matrix created from an angle and an axis vector(3).
//! @note Rotation angle is expressed in radiants!
//! @note axis vector should be normalized!
//! @param rad rotation angle
//! @param axis rotation axis
//! @param m input matrix
//! @return rotated matrix
static inline glmath_mat4 glmath_mat4_rotate(glmath_mat4 m, glmath_real rad, glmath_vec3 axis);
	
//! Multiplicates a 4x4 matrix by a 4x4 rotation matrix created from an angle and the x-axis as axis vector.
//! @note Rotation angle is expressed in radiants!
//! @param rad rotation angle
//! @param m input matrix
//! @return rotated matrix
static inline glmath_mat4 glmath_mat4_xrotate(glmath_mat4 m, glmath_real rad);
	
//! Multiplicates a 4x4 matrix by a 4x4 rotation matrix created from an angle and the y-axis as axis vector.
//! @note Rotation angle is expressed in radiants!
//! @param rad rotation angle
//! @param m input matrix
//! @return rotated matrix
static inline glmath_mat4 glmath_mat4_yrotate(glmath_mat4 m, glmath_real rad);
	
//! Multiplicates a 4x4 matrix by a 4x4 rotation matrix created from an angle and the z-axis as axis vector.
//! @note Rotation angle is expressed in radiants!
//! @param rad rotation angle
//! @param m input matrix
//! @return rotated matrix
static inline glmath_mat4 glmath_mat4_zrotate(glmath_mat4 m, glmath_real rad);
	
//! Multiplicates a 4x4 matrix by a 4x4 rotation matrix created from euler angles.
//! @param m input matrix
//! @param a rotation angles
//! @return rotaion matrix
static inline glmath_mat4 glmath_mat4_rotate_euler_angles(glmath_mat4 m, glmath_euler_angles a);
	
//! Creates a perspective matrix from given arguments.
//! @note fov in radiants
//! @param fov Field Of View.
//! @param aspect Aspect ratio (width/height).
//! @param near Distance to near clipping plane.
//! @param far Distance to far clipping plane.
//! @return projection matrix
static inline glmath_mat4 glmath_mat4_create_perspective(glmath_real fov, glmath_real aspect, glmath_real near, glmath_real far);
	
//! Creates a frustum projection matrix from given arguments.
//! @param near Distance to near clipping plane.
//! @param far Distance to far clipping plane.
//! @return frustum matrix
static inline glmath_mat4 glmath_mat4_create_frustum(glmath_real near, glmath_real far);
	
//! Creates a ortogonal projection matrix from given arguments.
//! @param left right top bottom Bounding parameters.
//! @param near Distance to near clipping plane.
//! @param far Distance to far clipping plane.
//! @return orthogonal matrix
static inline glmath_mat4 glmath_mat4_create_ortho(glmath_real left, glmath_real right, glmath_real top, glmath_real bottom, glmath_real near, glmath_real far);
	
//! Creates a view matrix from given components.
//! @param eyeX eyeY eyeZ eye/camera position
//! @param centerX centerY centerZ reference position
//! @param upX upY upZ up vector direction
//! @return view matrix
static inline glmath_mat4 glmath_mat4_create_lookat(glmath_real eyeX, glmath_real eyeY, glmath_real eyeZ, glmath_real centerX, glmath_real centerY, glmath_real centerZ, glmath_real upX, glmath_real upY, glmath_real upZ);
	
//! Creates a view matrix from given vectors(3).
//! @param eye eye/camera position
//! @param center reference point
//! @param up up vector
//! @return view matrix
static inline glmath_mat4 glmath_mat4_create_lookat_vec3(glmath_vec3 eye, glmath_vec3 center, glmath_vec3 up);
	
//! Creates a view matrix from given vectors(3).
//! @param p eye/camera position
//! @param f forward vector
//! @param u up vector
//! @return view matrix
static inline glmath_mat4 glmath_mat4_create_view(glmath_vec3 p, glmath_vec3 f, glmath_vec3 u);
	
//! Decomposes a matrix(4) into it's transformations parameters.
//! @note Works only with positiv scale factors.
//! @param m matrix to decompose
//! @return Transform parameters
static inline glmath_transform_params glmath_mat4_decompose(glmath_mat4 m);
	
//! Multiplicates a 4x4 matrix with a vector(4).
//! @param m matrix
//! @param v vector
//! @return resulting vector
static inline glmath_vec4 glmath_mat4_mul_vec4(glmath_mat4 m, glmath_vec4 v);
	
//! Adds two 4x4 matrices.
//! @param m1 left matrix
//! @param m2 right matrix
//! @return resulting matrix
static inline glmath_mat4 glmath_mat4_add(glmath_mat4 m1, glmath_mat4 m2);
	
//! Subtracts two 4x4 matrices.
//! @param m1 left matrix
//! @param m2 right matrix
//! @return resulting matrix
static inline glmath_mat4 glmath_mat4_sub(glmath_mat4 m1, glmath_mat4 m2);
	
//! Multiplicates two 4x4 matrices.
//! @param m1 left matrix
//! @param m2 right matrix
//! @return resulting matrix
static inline glmath_mat4 glmath_mat4_mul(glmath_mat4 m1, glmath_mat4 m2);
	
#include "glmath_mat4.inl"
	
#ifdef __cplusplus
}
#endif

#endif