//===========================================================================
/*
 *  glmath - OpenGL Mathematics Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file glmath_mat3.h
 *	@date 17.01.15.
 *  @author Armon Carigiet
 *
 *  @brief Provides algebra and transformation functions for 3x3 matrices (glmath_mat3).
 *  @see glmath_types.h
 */
//===========================================================================

#ifndef glmath_glmath_mat3_h
#define glmath_glmath_mat3_h

#include "glmath_types.h"
#include "glmath_vec3.h"
#include "glmath_quat.h"

#ifdef __cplusplus
extern "C" {
#endif
	
//! Builds a 3x3 identity matrix.
//! @return identity matrix
static inline glmath_mat3 glmath_mat3_create_identity();
	
//! Builds a 3x3 matrix from matrix components.
//! @param m00-m22 matrix components
//! @return matrix
static inline glmath_mat3 glmath_mat3_create(glmath_real m00, glmath_real m01, glmath_real m02, glmath_real m10, glmath_real m11, glmath_real m12, glmath_real m20, glmath_real m21, glmath_real m22);
	
//! Builds a 3x3 matrix from a quaternion.
//! @param q quaternion
//! @return matrix
static inline glmath_mat3 glmath_mat3_create_quat(glmath_quat q);
	
//! Transposes a 3x3 matrix.
//! @param m matrix
//! @return transposed matrix
static inline glmath_mat3 glmath_mat3_transpose(glmath_mat3 m);
	
//! Calculates the determinant of a 3x3 matrix.
//! @param m matrix
//! @return determinant
static inline glmath_real glmath_mat3_det(glmath_mat3 m);
	
//! Inverts a 3x3 matrix.
//! @param m matrix
//! @param invertable is set to true if m is invertable, else to false.
//! @return inverted matrix
static inline glmath_mat3 glmath_mat3_invert(glmath_mat3 m, int* invertable);
	
//! Builds a 3x3 scale matrix from components.
//! @param x x scale component
//! @param y y scale component
//! @param z z scale component
//! @return scale matrix
static inline glmath_mat3 glmath_mat3_create_scale(glmath_real x, glmath_real y, glmath_real z);
	
//! Builds a 3x3 scale matrix from a vector(3).
//! @param v scale vector
//! @return scale matrix
static inline glmath_mat3 glmath_mat3_create_scale_vec3(glmath_vec3 v);
	
//! Multiplicates a 3x3 matrix by a 3x3 scale matrix created from components.
//! @param m input matrix
//! @param x x scale component
//! @param y y scale component
//! @param z z scale component
//! @return scale matrix
static inline glmath_mat3 glmath_mat3_scale(glmath_mat3 m, glmath_real x, glmath_real y, glmath_real z);
	
//! Multiplicates a 3x3 matrix by a 3x3 scale matrix created from an vector(3).
//! @param m input matrix
//! @param v scale vector
static inline glmath_mat3 glmath_mat3_scale_vec3(glmath_mat3 m, glmath_vec3 v);
	
//! Builds a 3x3 rotation matrix from an angle and an axis vector(3).
//! @note Rotation angle is expressed in radiants!
//! @note axis vector should be normalized!
//! @param rad rotation angle
//! @param axis rotaion axis
//! @return rotation matrix
static inline glmath_mat3 glmath_mat3_create_rotation(glmath_real rad, glmath_vec3 axis);
	
//! Builds a 3x3 rotation matrix from an angle and the x-axis as axis vector(3).
//! @note Rotation angle is expressed in radiants!
//! @param rad rotation angle
//! @return rotation matrix
static inline glmath_mat3 glmath_mat3_create_xrotation(glmath_real rad);
	
//! Builds a 3x3 rotation matrix from an angle and the y-axis as axis vector(3).
//! @note Rotation angle is expressed in radiants!
//! @param rad rotation angle
//! @return rotation matrix
static inline glmath_mat3 glmath_mat3_create_yrotation(glmath_real rad);
	
//! Builds a 3x3 rotation matrix from an angle and the z-axis as axis vector(3).
//! @note Rotation angle is expressed in radiants!
//! @param rad rotation angle
//! @return rotation matrix
static inline glmath_mat3 glmath_mat3_create_zrotation(glmath_real rad);
	
//! Builds a 3x3 rotation matrix from euler angles.
//! @param a rotation angles
//! @return rotaion matrix
static inline glmath_mat3 glmath_mat3_create_rotation_euler_angles(glmath_euler_angles a);
	
//! Multiplicates a 3x3 matrix by a 3x3 rotation matrix created from an angle and an axis vector(3).
//! @note Rotation angle is expressed in radiants!
//! @note axis vector should be normalized!
//! @param rad rotation angle
//! @param axis rotation axis
//! @param m input matrix
//! @return rotated matrix
static inline glmath_mat3 glmath_mat3_rotate(glmath_mat3 m, glmath_real rad, glmath_vec3 axis);
	
//! Multiplicates a 3x3 matrix by a 3x3 rotation matrix created from an angle and the x-axis as axis vector.
//! @note Rotation angle is expressed in radiants!
//! @param rad rotation angle
//! @param m input matrix
//! @return rotated matrix
static inline glmath_mat3 glmath_mat3_xrotate(glmath_mat3 m, glmath_real rad);
	
//! Multiplicates a 3x3 matrix by a 3x3 rotation matrix created from an angle and the y-axis as axis vector.
//! @note Rotation angle is expressed in radiants!
//! @param rad rotation angle
//! @param m input matrix
//! @return rotated matrix
static inline glmath_mat3 glmath_mat3_yrotate(glmath_mat3 m, glmath_real rad);
	
//! Multiplicates a 3x3 matrix by a 3x3 rotation matrix created from an angle and the z-axis as axis vector.
//! @note Rotation angle is expressed in radiants!
//! @param rad rotation angle
//! @param m input matrix
//! @return rotated matrix
static inline glmath_mat3 glmath_mat3_zrotate(glmath_mat3 m, glmath_real rad);
	
//! Multiplicates a 3x3 matrix by a 3x3 rotation matrix created from euler angles.
//! @param m input matrix
//! @param a rotaion angles
//! @return rotaion matrix
static inline glmath_mat3 glmath_mat3_rotate_euler_angles(glmath_mat3 m, glmath_euler_angles a);
	
//! Multiplicates a 3x3 matrix with a vector(3).
//! @param m matrix
//! @param v vector
//! @return resulting vector
static inline glmath_vec3 glmath_mat3_mul_vec3(glmath_mat3 m, glmath_vec3 v);
	
//! Adds two 3x3 matrices.
//! @param m1 left matrix
//! @param m2 right matrix
//! @return resulting matrix
static inline glmath_mat3 glmath_mat3_add(glmath_mat3 m1, glmath_mat3 m2);
	
//! Subtracts two 3x3 matrices.
//! @param m1 left matrix
//! @param m2 right matrix
//! @return resulting matrix
static inline glmath_mat3 glmath_mat3_sub(glmath_mat3 m1, glmath_mat3 m2);
	
//! Multiplicates two 3x3 matrices.
//! @param m1 left matrix
//! @param m2 right matrix
//! @return resulting matrix
static inline glmath_mat3 glmath_mat3_mul(glmath_mat3 m1, glmath_mat3 m2);
	
#include "glmath_mat3.inl"
	
#ifdef __cplusplus
}
#endif

#endif