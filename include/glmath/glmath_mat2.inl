//===========================================================================
/*
 *  glmath - OpenGL Mathematics Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file glmath_mat2.inl
 *	@date 17.01.15.
 *  @author Armon Carigiet
 *  @see glmath_mat2.h
 */
//===========================================================================

static inline glmath_mat2 glmath_mat2_create_identity(){
	glmath_mat2 m={{
		1.0f, 0.0f,
		0.0f, 1.0f
	}};
	return m;
}

static inline glmath_mat2 glmath_mat2_create(glmath_real m00, glmath_real m01, glmath_real m10, glmath_real m11){
	glmath_mat2 m={{
		m00, m01,
		m10, m11
	}};
	return m;
}

static inline glmath_mat2 glmath_mat2_transpose(glmath_mat2 m){
	glmath_mat2 mr={{
		mr.m[0], mr.m[2],
		mr.m[1], mr.m[3]
	}};
	return mr;
}

static inline glmath_real glmath_mat2_det(glmath_mat2 m){
	return m.m[0]*m.m[3] - m.m[1]*m.m[2];
}

static inline glmath_mat2 glmath_mat2_invert(glmath_mat2 m, bool* invertable){
	glmath_real det=glmath_mat2_det(m);
	glmath_real inv;
	glmath_mat2 mr;
	if(det){
		mr.m[0]=m.m[3]/det;
		mr.m[1]=-m.m[1]/det;
		mr.m[2]=-m.m[2]/det;
		mr.m[3]=m.m[0]/det;
		inv=true;
	}else{
		mr.m[0]=NAN;
		mr.m[1]=NAN;
		mr.m[2]=NAN;
		mr.m[3]=NAN;
		inv=false;
	}
	return mr;
}

static inline glmath_mat2 glmath_mat2_create_scale(glmath_real x, glmath_real y){
	glmath_mat2 mr={{
		x, 0,
		0, y
	}};
	return mr;
}

static inline glmath_mat2 glmath_mat2_create_scale_vec2(glmath_vec2 v){
	glmath_mat2 mr={{
		v.v[0], 0,
		0, v.v[1]
	}};
	return mr;
}

static inline glmath_mat2 glmath_mat2_scale(glmath_mat2 m, glmath_real x, glmath_real y){
	glmath_mat2 mr=glmath_mat2_create_scale(x, y);
	mr=glmath_mat2_mul(m, mr);
	return mr;
}

static inline glmath_mat2 glmath_mat2_scale_vec2(glmath_mat2 m, glmath_vec2 v){
	glmath_mat2 mr=glmath_mat2_create_scale_vec2(v);
	mr=glmath_mat2_mul(m, mr);
	return mr;
}

static inline glmath_mat2 glmath_mat2_create_rotation(glmath_real rad){
	glmath_real cos=glmath_cos(rad);
	glmath_real sin=glmath_sin(rad);
	glmath_mat2 m={{
		cos, sin,
		-sin, cos
	}};
	return m;
}

static inline glmath_mat2 glmath_mat2_rotate(glmath_mat2 m, glmath_real rad){
	glmath_mat2 mr=glmath_mat2_create_rotation(rad);
	mr=glmath_mat2_mul(m, mr);
	return mr;
}

static inline glmath_vec2 glmath_mat2_rotate_vec2(glmath_vec2 v, glmath_vec2 p, glmath_mat2 m){
	glmath_vec2 rv=glmath_vec2_sub(v, p);
	rv=glmath_mat2_mul_vec2(m, rv);
	rv=glmath_vec2_add(rv, p);
	return rv;
}

static inline glmath_vec2 glmath_mat2_mul_vec2(glmath_mat2 m, glmath_vec2 v){
	glmath_vec2 vr={{
		m.m[0]*v.v[0] + m.m[1]*v.v[1],
		m.m[2]*v.v[0] + m.m[3]*v.v[1]
	}};
	return vr;
}

static inline glmath_mat2 glmath_mat2_add(glmath_mat2 m1, glmath_mat2 m2){
	glmath_mat2 m;
#if defined(GLMATH_SINGLE_PRECISION) && defined(GLMATH_SSE)
	_mm_store_ps(&m.m[0], _mm_add_ps(_mm_load_ps(&m1.m[0]), _mm_load_ps(&m2.m[0])));
#elif defined(GLMATH_DOUBLE_PRECISION) && defined(GLMATH_AVX)
	_mm256_store_pd(&m.m[0], _mm256_add_pd(_mm256_load_pd(&m1.m[0]), _mm256_load_pd(&m2.m[0])));
#elif defined(GLMATH_DOUBLE_PRECISION) && defined(GLMATH_SSE)
	_mm_store_pd(&m.m[0], _mm_add_pd(_mm_load_pd(&m1.m[0]), _mm_load_pd(&m2.m[0])));
	_mm_store_pd(&m.m[2], _mm_add_pd(_mm_load_pd(&m1.m[2]), _mm_load_pd(&m2.m[02])));
#else
	m.m[0]= m1.m[0] + m2.m[0];
	m.m[1]= m1.m[1] + m2.m[1];
	m.m[2]= m1.m[2] + m2.m[2];
	m.m[3]= m1.m[3] + m2.m[3];
#endif
	return m;
}

static inline glmath_mat2 glmath_mat2_sub(glmath_mat2 m1, glmath_mat2 m2){
	glmath_mat2 m;
#if defined(GLMATH_SINGLE_PRECISION) && defined(GLMATH_SSE)
	_mm_store_ps(&m.m[0], _mm_sub_ps(_mm_load_ps(&m1.m[0]), _mm_load_ps(&m2.m[0])));
#elif defined(GLMATH_DOUBLE_PRECISION) && defined(GLMATH_AVX)
	_mm256_store_pd(&m.m[0], _mm256_sub_pd(_mm256_load_pd(&m1.m[0]), _mm256_load_pd(&m2.m[0])));
#elif defined(GLMATH_DOUBLE_PRECISION) && defined(GLMATH_SSE)
	_mm_store_pd(&m.m[0], _mm_sub_pd(_mm_load_pd(&m1.m[0]), _mm_load_pd(&m2.m[0])));
	_mm_store_pd(&m.m[2], _mm_sub_pd(_mm_load_pd(&m1.m[2]), _mm_load_pd(&m2.m[2])));
#else
	m.m[0]= m1.m[0] + m2.m[0];
	m.m[1]= m1.m[1] + m2.m[1];
	m.m[2]= m1.m[2] + m2.m[2];
	m.m[3]= m1.m[3] + m2.m[3];
#endif
	return m;
}

static inline glmath_mat2 glmath_mat2_mul(glmath_mat2 m1, glmath_mat2 m2){
	glmath_mat2 m={
		m1.m[0]*m2.m[0] + m1.m[1]*m2.m[2],
		m1.m[0]*m2.m[1] + m1.m[1]*m2.m[3],
		m1.m[2]*m2.m[0] + m1.m[3]*m2.m[2],
		m1.m[2]*m2.m[1] + m1.m[3]*m2.m[3]
	};
	return m;
}
