//===========================================================================
/*
 *  glmath - OpenGL Mathematics Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file glmath_mat3.inl
 *	@date 17.03.14.
 *  @author Armon Carigiet
 *  @see glmath_mat3.h
 */
//===========================================================================

static inline glmath_mat3 glmath_mat3_create_identity(){
	glmath_mat3 m={{
		1.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 1.0f
	}};
	return m;
}

static inline glmath_mat3 glmath_mat3_create(glmath_real m00, glmath_real m01, glmath_real m02, glmath_real m10, glmath_real m11, glmath_real m12, glmath_real m20, glmath_real m21, glmath_real m22){
	glmath_mat3 m={{
		m00, m01, m02,
		m10, m11, m12,
		m20, m21, m22
	}};
	return m;
}

static inline glmath_mat3 glmath_mat3_create_quat(glmath_quat q){
	q=glmath_quat_norm(q);
	glmath_real x2=q.q[1]*q.q[1];
	glmath_real y2=q.q[2]*q.q[2];
	glmath_real z2=q.q[3]*q.q[3];
	glmath_mat3 mat={{
        1-2*(y2+z2), 2*(q.q[1]*q.q[2] - q.q[0]*q.q[3]), 2*(q.q[1]*q.q[3] + q.q[0]*q.q[2]),
        2*(q.q[1]*q.q[2] + q.q[0]*q.q[3]), 1-2*(x2+z2), 2*(q.q[2]*q.q[3] - q.q[0]*q.q[1]),
        2*(q.q[1]*q.q[3] - q.q[0]*q.q[2]), 2*(q.q[2]*q.q[3] + q.q[0]*q.q[1]), 1-2*(x2+y2)
	}};
	return mat;
}

static inline glmath_mat3 glmath_mat3_transpose(glmath_mat3 m){
	glmath_mat3 m1={{
		m.m[0], m.m[3], m.m[6],
		m.m[1], m.m[4], m.m[7],
		m.m[2], m.m[5], m.m[8],
	}};
	return m1;
}

static inline glmath_real glmath_mat3_det(glmath_mat3 m){
	return m.m[0]*m.m[4]*m.m[8] + m.m[1]*m.m[5]*m.m[6] + m.m[2]*m.m[3]*m.m[7] - m.m[2]*m.m[4]*m.m[6] - m.m[1]*m.m[3]*m.m[8] - m.m[0]*m.m[5]*m.m[7];
}

static inline glmath_mat3 glmath_mat3_invert(glmath_mat3 m, int* invertable){
	glmath_real det=glmath_mat3_det(m);
	glmath_mat3 mr;
	if(det==0){
		*invertable=false;
	}else{
		mr.m[0]=(m.m[4]*m.m[8]-m.m[7]*m.m[5])/det;
		mr.m[1]=(m.m[2]*m.m[7]-m.m[1]*m.m[8])/det;
		mr.m[2]=(m.m[1]*m.m[5]-m.m[2]*m.m[4])/det;
		mr.m[3]=(m.m[5]*m.m[6]-m.m[3]*m.m[8])/det;
		mr.m[4]=(m.m[0]*m.m[8]-m.m[2]*m.m[6])/det;
		mr.m[5]=(m.m[2]*m.m[3]-m.m[0]*m.m[5])/det;
		mr.m[6]=(m.m[3]*m.m[7]-m.m[4]*m.m[6])/det;
		mr.m[7]=(m.m[1]*m.m[6]-m.m[0]*m.m[7])/det;
		mr.m[8]=(m.m[0]*m.m[4]-m.m[1]*m.m[3])/det;
		*invertable=true;
	}
	
	return mr;
}

static inline glmath_mat3 glmath_mat3_create_scale(glmath_real x, glmath_real y, glmath_real z){
	glmath_mat3 m={{
		x, 0, 0,
		0, y, 0,
		0, 0, z
	}};
	return m;
}

static inline glmath_mat3 glmath_mat3_create_scale_vec3(glmath_vec3 v){
	glmath_mat3 m={{
		v.x, 0, 0,
		0, v.y, 0,
		0, 0, v.z
	}};
	return m;
}

static inline glmath_mat3 glmath_mat3_scale(glmath_mat3 m, glmath_real x, glmath_real y, glmath_real z){
	glmath_mat3 mr=glmath_mat3_create_scale(x, y, z);
	mr=glmath_mat3_mul(m, mr);
	return mr;
}

static inline glmath_mat3 glmath_mat3_scale_vec3(glmath_mat3 m, glmath_vec3 v){
	glmath_mat3 mr=glmath_mat3_create_scale_vec3(v);
	mr=glmath_mat3_mul(m, mr);
	return mr;
}

static inline glmath_mat3 glmath_mat3_create_rotation(glmath_real rad, glmath_vec3 axis){
	glmath_real sin=glmath_sin(rad);
	glmath_real cos=glmath_cos(rad);
	glmath_real cos2=1-cos;
	axis=glmath_vec3_norm(axis);
	
	glmath_mat3 m={{
		axis.v[0]*axis.v[0]*cos2 + cos, axis.v[1]*axis.v[0]*cos2 - axis.v[2]*sin, axis.v[2]*axis.v[0]*cos2 + axis.v[1]*sin,
		axis.v[0]*axis.v[1]*cos2 + axis.v[2]*sin, axis.v[1]*axis.v[1]*cos2 + cos, axis.v[2]*axis.v[1]*cos2 - axis.v[0]*sin,
		axis.v[0]*axis.v[2]*cos2 - axis.v[1]*sin, axis.v[1]*axis.v[2]*cos2 + axis.v[0]*sin, axis.v[2]*axis.v[2]*cos2 + cos,
	}};
	return m;
}


static inline glmath_mat3 glmath_mat3_create_xrotation(glmath_real rad){
	glmath_real sin=glmath_sin(rad);
	glmath_real cos=glmath_cos(rad);
	glmath_mat3 m={{
		1, 0, 0,
		0, cos, sin,
		0, -sin, cos
	}};
	return m;
}

static inline glmath_mat3 glmath_mat3_create_yrotation(glmath_real rad){
	glmath_real sin=glmath_sin(rad);
	glmath_real cos=glmath_cos(rad);
	glmath_mat3 m={{
		cos,  0, -sin,
		0, 	  1, 0,
		sin, 0, cos
	}};
	return m;
}

static inline glmath_mat3 glmath_mat3_create_zrotation(glmath_real rad){
	glmath_real sin=glmath_sin(rad);
	glmath_real cos=glmath_cos(rad);
	glmath_mat3 m={{
		cos, sin, 0,
		-sin,  cos, 0,
		0,      0, 1
	}};
	return m;
}

static inline glmath_mat3 glmath_mat3_create_rotation_euler_angles(glmath_euler_angles a){
	glmath_mat3 mr=glmath_mat3_create_identity();
	mr=glmath_mat3_mul(mr, glmath_mat3_create_xrotation(a.r[0]));
	mr=glmath_mat3_mul(mr, glmath_mat3_create_yrotation(a.r[1]));
	mr=glmath_mat3_mul(mr, glmath_mat3_create_zrotation(a.r[2]));
	return mr;
}

static inline glmath_mat3 glmath_mat3_rotate(glmath_mat3 m, glmath_real rad, glmath_vec3 axis){
	glmath_mat3 mr=glmath_mat3_create_rotation(rad, axis);
	mr=glmath_mat3_mul(m, mr);
	return mr;
}

static inline glmath_mat3 glmath_mat3_xrotate(glmath_mat3 m, glmath_real rad){
	glmath_mat3 mr=glmath_mat3_create_xrotation(rad);
	mr=glmath_mat3_mul(m, mr);
	return mr;
}

static inline glmath_mat3 glmath_mat3_yrotate(glmath_mat3 m, glmath_real rad){
	glmath_mat3 mr=glmath_mat3_create_yrotation(rad);
	mr=glmath_mat3_mul(m, mr);
	return mr;
}

static inline glmath_mat3 glmath_mat3_zrotate(glmath_mat3 m, glmath_real rad){
	glmath_mat3 mr=glmath_mat3_create_zrotation(rad);
	mr=glmath_mat3_mul(m, mr);
	return mr;
}

static inline glmath_mat3 glmath_mat3_rotate_euler_angles(glmath_mat3 m, glmath_euler_angles a){
	glmath_mat3 mr=glmath_mat3_create_identity();
	mr=glmath_mat3_mul(mr, glmath_mat3_create_xrotation(a.r[0]));
	mr=glmath_mat3_mul(mr, glmath_mat3_create_yrotation(a.r[1]));
	mr=glmath_mat3_mul(mr, glmath_mat3_create_zrotation(a.r[2]));
	return glmath_mat3_mul(m, mr);
}

static inline glmath_vec3 glmath_mat3_mul_vec3(glmath_mat3 m, glmath_vec3 v){
	glmath_vec3 vr={{
		m.m[0]*v.v[0] + m.m[1]*v.v[1] + m.m[2]*v.v[2],
		m.m[3]*v.v[0] + m.m[4]*v.v[1] + m.m[5]*v.v[2],
		m.m[6]*v.v[0] + m.m[7]*v.v[1] + m.m[8]*v.v[2]
	}};
	return vr;
}

static inline glmath_mat3 glmath_mat3_add(glmath_mat3 m1, glmath_mat3 m2){
	glmath_mat3 m;
#if defined(GLMATH_SINGLE_PRECISION) && defined(GLMATH_AVX)
	_mm256_storeu_ps(&m.m[0], _mm256_add_ps(_mm256_loadu_ps(&m1.m[0), _mm256_loadu_ps(&m2.m[0])));
#elif defined(GLMATH_SINGLE_PRECISION) && defined(GLMATH_SSE)
	_mm_storeu_ps(&m.m[0], _mm_add_ps(_mm_loadu_ps(&m1.m[0]), _mm_loadu_ps(&m2.m[0])));
	_mm_storeu_ps(&m.m[4],  _mm_add_ps(_mm_loadu_ps(&m1.m[4]), _mm_loadu_ps(&m2.m[4])));
#elif defined(GLMATH_DOUBLE_PRECISION) && defined(GLMATH_AVX)
	_mm256_storeu_pd(&m.m[0], _mm256_add_pd(_mm256_loadu_pd(&m1.m[0), _mm256_loadu_pd(&m2.m[0])));
	_mm256_storeu_pd(&m.m[4], _mm256_add_pd(_mm256_loadu_pd(&m1.m[4), _mm256_loadu_pd(&m2.m[4])));
#else
	m.m[0]= m1.m[0] + m2.m[0];
	m.m[1]= m1.m[1] + m2.m[1];
	m.m[2]= m1.m[2] + m2.m[2];
	m.m[3]= m1.m[3] + m2.m[3];
	m.m[4]= m1.m[4] + m2.m[4];
	m.m[5]= m1.m[5] + m2.m[5];
	m.m[6]= m1.m[6] + m2.m[6];
	m.m[7]= m1.m[7] + m2.m[7];
#endif
	m.m[8]= m1.m[8] + m2.m[8];
	return m;
}

static inline glmath_mat3 glmath_mat3_sub(glmath_mat3 m1, glmath_mat3 m2){
	glmath_mat3 m;
																
#if defined(GLMATH_SINGLE_PRECISION) && defined(GLMATH_AVX)
	_mm256_storeu_ps(&m.m[0], _mm256_sub_ps(_mm256_loadu_ps(&m1.m[0), _mm256_loadu_ps(&m2.m[0])));
#elif defined(GLMATH_SINGLE_PRECISION) && defined(GLMATH_SSE)
	_mm_storeu_ps(&m.m[0], _mm_sub_ps(_mm_loadu_ps(&m1.m[0]), _mm_loadu_ps(&m2.m[0])));
	_mm_storeu_ps(&m.m[4],  _mm_sub_ps(_mm_loadu_ps(&m1.m[4]), _mm_loadu_ps(&m2.m[4])));
#elif defined(GLMATH_DOUBLE_PRECISION) && defined(GLMATH_AVX)
	_mm256_storeu_pd(&m.m[0], _mm256_sub_pd(_mm256_loadu_pd(&m1.m[0), _mm256_loadu_pd(&m2.m[0])));
	_mm256_storeu_pd(&m.m[4], _mm256_sub_pd(_mm256_loadu_pd(&m1.m[4), _mm256_loadu_pd(&m2.m[4])));
#else
	m.m[0]= m1.m[0] - m2.m[0];
	m.m[1]= m1.m[1] - m2.m[1];
	m.m[2]= m1.m[2] - m2.m[2];
	m.m[3]= m1.m[3] - m2.m[3];
	m.m[4]= m1.m[4] - m2.m[4];
	m.m[5]= m1.m[5] - m2.m[5];
	m.m[6]= m1.m[6] - m2.m[6];
	m.m[7]= m1.m[7] - m2.m[7];
#endif
	m.m[8]= m1.m[8] - m2.m[8];
	return m;
}

static inline glmath_mat3 glmath_mat3_mul(glmath_mat3 m1, glmath_mat3 m2){
	glmath_mat3 m;
	
	m.m[0]= m1.m[0]*m2.m[0] + m1.m[1]*m2.m[3] + m1.m[2]*m2.m[6];
	m.m[1]= m1.m[0]*m2.m[1] + m1.m[1]*m2.m[4] + m1.m[2]*m2.m[7];
	m.m[2]= m1.m[0]*m2.m[2] + m1.m[1]*m2.m[5] + m1.m[2]*m2.m[8];
	
	m.m[3]= m1.m[3]*m2.m[0] + m1.m[4]*m2.m[3] + m1.m[5]*m2.m[6];
	m.m[4]= m1.m[3]*m2.m[1] + m1.m[4]*m2.m[4] + m1.m[5]*m2.m[7];
	m.m[5]= m1.m[3]*m2.m[2] + m1.m[4]*m2.m[5] + m1.m[5]*m2.m[8];
	
	m.m[6]= m1.m[6]*m2.m[0] + m1.m[7]*m2.m[3] + m1.m[8]*m2.m[6];
	m.m[7]= m1.m[6]*m2.m[1] + m1.m[7]*m2.m[4] + m1.m[8]*m2.m[7];
	m.m[8]= m1.m[6]*m2.m[2] + m1.m[7]*m2.m[5] + m1.m[8]*m2.m[8];
	return m;
}