//===========================================================================
/*
 *  glmath - OpenGL Mathematics Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file glmath_constants.h
 *	@date 13.04.2015
 *  @author Armon Carigiet
 *
 *  @brief Provides a variety of matematical constants.
 */
//===========================================================================

#ifndef glmathlib_glmath_constants_h
#define glmathlib_glmath_constants_h

#include "complex.h"

#define GLMATH_PI 				3.14159265359
#define GLMATH_2PI				6.28318530718
#define GLMATH_4PI				12.5663706144

#define GLMATH_E  				2.71828182845

#define GLMATH_SQRT2 			1.414213562373
#define GLMATH_SQRT3			1.732050807568
#define GLMATH_SQRT5			2.236067977499

#define GLMARH_4ROOT5			1.495348781221
#define GLMATH_EROOTE			1.444667861009

#define GLMATH_LN2				0.693147180559

#define GLMATH_COMPLEX_I 		_Complex_I
#define GLMATH_I 				GLMATH_COMPEX_I
#define GLMATH_SQRT_I_SCALAR	0.707106781186
#define GLMATH_SQRT_I			GLMATH_SQRT_I_SCALAR + GLMATH_SQRT_I_SCALAR*GLMATH_I

#define GLMATH_1DEG_IN_RAD 		0.01745329251996
#define GLMATH_1RAD_IN_DEG 		57.2957795130822

#define GLMATH_GOLDEN_RATIO		1.61803398874

#define GLMATH_GAMA_QUARTER		3.62560990822

#define GLMATH_MAGIC_ANGLE		0.95531661812	// arctan(sqrt(2))

// units

#define GLMATH_YOTTA			(+1.0E+24)
#define GLMATH_ZETTA			(+1.0E+21)
#define GLMATH_EXA				(+1.0E+18)
#define GLMATH_PETA				(+1.0E+15)
#define GLMATH_TERA				(+1.0E+12)
#define GLMATH_GIGA				(+1.0E+9)
#define GLMATH_MEGA				(+1.0E+6)
#define GLMATH_KILO				(+1.0E+3)
#define GLMATH_HECTO			(+1.0E+2)
#define GLMATH_DECA				(+1.0E+1)

#define GLMATH_DECI				(+1.0E-1)
#define GLMATH_CENTI			(+1.0E-2)
#define GLMATH_MILLI			(+1.0E-3)
#define GLMATH_MICRO			(+1.0E-6)
#define GLMATH_NANO				(+1.0E-9)
#define GLMATH_PICO				(+1.0E-12)
#define GLMATH_FEMTO			(+1.0E-15)
#define GLMATH_ATTO				(+1.0E-18)
#define GLMATH_ZEPTO			(+1.0E-21)
#define GLMATH_YOKTO			(+1.0E-24)


#endif