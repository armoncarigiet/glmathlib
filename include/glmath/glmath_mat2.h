//===========================================================================
/*
 *  glmath - OpenGL Mathematics Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file glmath_mat2.h
 *	@date 17.01.15.
 *  @author Armon Carigiet
 *
 *  @brief Provides algebra and transformation functions for 2x2 matrices (glmath_mat2).
 *  @see glmath_types.h
 */
//===========================================================================

#ifndef glmath_glmath_mat2_h
#define glmath_glmath_mat2_h

#include "glmath_types.h"
#include "glmath_vec2.h"

#ifdef __cplusplus
extern "C" {
#endif
	
//! Builds a 2x2 identity matrix.
//! @return identity matrix
static inline glmath_mat2 glmath_mat2_create_identity();
	
//! Builds a 2x2 matrix from matrix components.
//! @param m00-m11 matrix components
//! @return matrix
static inline glmath_mat2 glmath_mat2_create(glmath_real m00, glmath_real m01, glmath_real m10, glmath_real m11);
	
//! Transposes a 2x2 matrix.
//! @param m matrix
//! @return transposed matrix
static inline glmath_mat2 glmath_mat2_transpose(glmath_mat2 m);
	
//! Calculates the determinant of a 2x2 matrix.
//! @param m matrix
//! @return determinant
static inline glmath_real glmath_mat2_det(glmath_mat2 m);
	
//! Inverts a 2x2 matrix.
//! @param m matrix
//! @param invertable is set to true if m is invertable, else to false.
//! @return inverted matrix
static inline glmath_mat2 glmath_mat2_invert(glmath_mat2 m, bool* invertable);
	
//! Builds a 2x2 scale matrix from components.
//! @param x x scale component
//! @param y y scale component
//! @return scale matrix
static inline glmath_mat2 glmath_mat2_create_scale(glmath_real x, glmath_real y);
	
//! Builds a 2x2 scale matrix from a vector(2).
//! @param v scale vector
//! @return scale matrix
static inline glmath_mat2 glmath_mat2_create_scale_vec2(glmath_vec2 v);
	
//! Multiplicates a 2x2 matrix by a 2x2 scale matrix created from components.
//! @param m input matrix
//! @param x x scale component
//! @param y y scale component
//! @return scale matrix
static inline glmath_mat2 glmath_mat2_scale(glmath_mat2 m, glmath_real x, glmath_real y);
	
//! Multiplicates a 2x2 matrix by a 2x2 scale matrix created from an vector(2).
//! @param m input matrix
//! @param v scale vector
static inline glmath_mat2 glmath_mat2_scale_vec2(glmath_mat2 m, glmath_vec2 v);
	
//! Builds a 2x2 rotation matrix from an angle.
//! @note Rotation angle is expressed in radiants!
//! @param rad rotation angle
//! @return rotation matrix
static inline glmath_mat2 glmath_mat2_create_rotation(glmath_real rad);
	
//! Multiplicates a 2x2 matrix by a 2x2 rotation matrix created from an angle.
//! @note Rotation angle is expressed in radiants!
//! @param rad rotation angle
//! @param m input matrix
//! @return rotated matrix
static inline glmath_mat2 glmath_mat2_rotate(glmath_mat2 m, glmath_real rad);
	
//! Rotates a vector around a point
//! @note Rotation angle is expressed in radiants!
//! @param v vector
//! @param p rotatio point
//! @param m rotation matrix
//! @return rotated matrix
static inline glmath_vec2 glmath_mat2_rotate_vec2(glmath_vec2 v, glmath_vec2 p, glmath_mat2 m);
	
//! Multiplicates a 2x2 matrix with a vector(2).
//! @param m matrix
//! @param v vector
//! @return resulting vector
static inline glmath_vec2 glmath_mat2_mul_vec2(glmath_mat2 m, glmath_vec2 v);
	
//! Adds two 2x2 matrices.
//! @param m1 left matrix
//! @param m2 right matrix
//! @return resulting matrix
static inline glmath_mat2 glmath_mat2_add(glmath_mat2 m1, glmath_mat2 m2);
	
//! Subtracts two 2x2 matrices.
//! @param m1 left matrix
//! @param m2 right matrix
//! @return resulting matrix
static inline glmath_mat2 glmath_mat2_sub(glmath_mat2 m1, glmath_mat2 m2);
	
//! Multiplicates two 2x2 matrices.
//! @param m1 left matrix
//! @param m2 right matrix
//! @return resulting matrix
static inline glmath_mat2 glmath_mat2_mul(glmath_mat2 m1, glmath_mat2 m2);
	
#include "glmath_mat2.inl"
	
#ifdef __cplusplus
}
#endif

#endif