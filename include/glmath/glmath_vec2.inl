//===========================================================================
/*
 *  glmath - OpenGL Mathematics Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file glmath_vec2.inl
 *	@date 06.10.2014
 *  @author Armon Carigiet
 *  @see glmath_vec2.h
 */
//===========================================================================

static inline glmath_vec2 glmath_vec2_create(glmath_real x, glmath_real y){
	glmath_vec2 v={{x,y}};
	return v;
}

static inline glmath_vec2 glmath_vec2_neg(glmath_vec2 v){
	glmath_vec2 vr={{
		-v.v[0],
		-v.v[1]
	}};
	return vr;
}

static inline glmath_vec2 glmath_vec2_add(glmath_vec2 v1, glmath_vec2 v2){
	glmath_vec2 v={{
		v1.v[0] + v2.v[0],
		v1.v[1] + v2.v[1]
	}};
	return v;
}

static inline glmath_vec2 glmath_vec2_sub(glmath_vec2 v1, glmath_vec2 v2){
	glmath_vec2 v={{
		v1.v[0] - v2.v[0],
		v1.v[1] - v2.v[1]
	}};
	return v;
}

static inline glmath_vec2 glmath_vec2_mul(glmath_vec2 v1, glmath_vec2 v2){
	glmath_vec2 v={{
		v1.v[0] * v2.v[0],
		v1.v[1] * v2.v[1]
	}};
	return v;
}

static inline glmath_vec2 glmath_vec2_div(glmath_vec2 v1, glmath_vec2 v2){
	glmath_vec2 v={{
		v1.v[0] / v2.v[0],
		v1.v[1] / v2.v[1]
	}};
	return v;
}

static inline glmath_vec2 glmath_vec2_add_s(glmath_vec2 v, glmath_real a){
	glmath_vec2 vr={{
		v.v[0] + a,
		v.v[1] + a
	}};
	return vr;
}

static inline glmath_vec2 glmath_vec2_sub_s(glmath_vec2 v, glmath_real a){
	glmath_vec2 vr={{
		v.v[0] - a,
		v.v[1] - a
	}};
	return vr;
}

static inline glmath_vec2 glmath_vec2_mul_s(glmath_vec2 v, glmath_real a){
	glmath_vec2 vr={{
		v.v[0] * a,
		v.v[1] * a
	}};
	return vr;
}

static inline glmath_vec2 glmath_vec2_div_s(glmath_vec2 v, glmath_real a){
	glmath_vec2 vr={{
		v.v[0] / a,
		v.v[1] / a
	}};
	return vr;
}

static inline glmath_real glmath_vec2_mag(glmath_vec2 v){
	return glmath_sqrt(v.v[0]*v.v[0] + v.v[1]*v.v[1]);
}

static inline glmath_real glmath_vec2_dist(glmath_vec2 v1, glmath_vec2 v2){
	return glmath_vec2_mag(glmath_vec2_sub(v2, v1));
}

static inline glmath_vec2 glmath_vec2_norm(glmath_vec2 v){
	glmath_real m=glmath_vec2_mag(v);
	return glmath_vec2_div_s(v, m);
}

static inline glmath_real glmath_vec2_dot(glmath_vec2 v1, glmath_vec2 v2){
	return v1.v[0]*v2.v[0] + v1.v[1]*v2.v[1];
}

static inline glmath_vec2 glmath_vec2_max(glmath_vec2 v1, glmath_vec2 v2){
	glmath_vec2 v;
	if(v1.v[0]<v2.v[0]){
		v.v[0]=v2.v[0];
	}else{
		v.v[0]=v1.v[0];
	}
	if(v1.v[1]<v2.v[1]){
		v.v[1]=v2.v[1];
	}else{
		v.v[1]=v1.v[1];
	}
	return v;
}

static inline glmath_vec2 glmath_vec2_min(glmath_vec2 v1, glmath_vec2 v2){
	glmath_vec2 v;
	if(v1.v[0]<v2.v[0]){
		v.v[0]=v1.v[0];
	}else{
		v.v[0]=v2.v[0];
	}
	if(v1.v[1]<v2.v[1]){
		v.v[1]=v1.v[1];
	}else{
		v.v[1]=v2.v[1];
	}
	return v;
}

static inline glmath_vec2 glmath_vec2_lerp(glmath_vec2 sv, glmath_vec2 ev, glmath_real a){
	glmath_vec2 v={{
		sv.v[0] + ((ev.v[0] - sv.v[0])*a),
		sv.v[1] + ((ev.v[1] - sv.v[1])*a)
	}};
    return v;
	
}

static inline glmath_vec2 glmath_vec2_proj(glmath_vec2 v, glmath_vec2 pv){
	glmath_real s = glmath_vec2_dot(pv, v) /  glmath_vec2_dot(pv, pv);
    glmath_vec2 vr = glmath_vec2_mul_s(pv, s);
    return vr;
}

static inline glmath_vec2 glmath_vec2_perp(glmath_vec2 v){
	glmath_vec2 vr={{
		v.v[1],
		v.v[0]
	}};
	return vr;
}