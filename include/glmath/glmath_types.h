//===========================================================================
/*
 *  glmath - OpenGL Mathematics Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file glmath_types.h
 *	@date 06.10.2014
 *  @author Armon Carigiet
 *
 *  @brief Provides all mathematical types.
 */
//===========================================================================


#ifndef glmathlib_glmath_types_h
#define glmathlib_glmath_types_h

#include <math.h>
#include <stdbool.h>
#include <stdint.h>

#include "glmath_extensions.h"

//#undef GLMATH_SSE
//#undef GLMATH_SSE3

#if defined(GLMATH_SINGLE_PRECISION) // SSE & AVX
	#define GLMATH_ALIGN __attribute__((aligned(16)))
	#define GLMATH_PACK 16
#elif defined(GLMATH_DOUBLE_PRECISION)
	#define GLMATH_ALIGN __attribute__((aligned(32)))
	#define GLMATH_PACK 32
#else
	#define GLMATH_ALIGN
#endif

#ifdef __cplusplus
extern "C" {
#endif

union _glmath_vec2{
	struct {
		glmath_real x;
		glmath_real y;
	};
	glmath_real v[2];
};
typedef union _glmath_vec2 glmath_vec2;

union _glmath_vec3{
	struct {
		glmath_real x;
		glmath_real y;
		glmath_real z;
	};
	struct {
		glmath_real r;
		glmath_real g;
		glmath_real b;
	};
	glmath_real v[3];
};
typedef union _glmath_vec3 glmath_vec3;

#if defined(_MSC_VER) && defined(GLMATH_PACK)
	#undef GLMATH_ALIGN
	#define GLMATH_ALIGN
	#pragma pack(push, GLMATH_PACK)
#endif
	
union _glmath_vec4{
	struct {
		glmath_real x;
		glmath_real y;
		glmath_real z;
		glmath_real w;
	};
	struct {
		glmath_real r;
		glmath_real g;
		glmath_real b;
		glmath_real a;
	};
	glmath_real v[4];
}GLMATH_ALIGN;
typedef union _glmath_vec4 glmath_vec4;

union _glmath_mat2{
	struct {
		glmath_real m00, m01;
		glmath_real m10, m11;
	};
	glmath_real m[4];
	glmath_real md[2][2];
	
}GLMATH_ALIGN;
typedef union _glmath_mat2 glmath_mat2;

#if defined(_MSC_VER) && defined(GLMATH_PACK)
	#pragma pack(pop)
#endif
	
union _glmath_mat3{
	struct {
		glmath_real m00, m01, m02;
		glmath_real m10, m11, m12;
		glmath_real m20, m21, m22;
	};
	glmath_real m[9];
	glmath_real md[3][3];
};
typedef union _glmath_mat3 glmath_mat3;
	
#if defined(_MSC_VER) && defined(GLMATH_PACK)
	#pragma pack(push, GLMATH_PACK)
#endif
	
union _glmath_mat4{
	struct{
		glmath_real m00, m01, m02, m03;
		glmath_real m10, m11, m12, m13;
		glmath_real m20, m21, m22, m23;
		glmath_real m30, m31, m32, m33;
	};
	glmath_real m[16];
	glmath_real md[4][4];
}GLMATH_ALIGN;
typedef union _glmath_mat4 glmath_mat4;
	
union _glmath_quat{
	struct{
		glmath_real w;
		glmath_real x;
		glmath_real y;
		glmath_real z;
	};
	struct{
		glmath_real s;
		glmath_vec3 v;
	};
	glmath_real q[4];
};
typedef union _glmath_quat glmath_quat;
	
#if defined(_MSC_VER) && defined(GLMATH_PACK)
	#pragma pack(pop)
#endif
	
union _glmath_euler_angles{
	struct{
		glmath_real x;
		glmath_real y;
		glmath_real z;
	};
	glmath_real r[3];
};
typedef union _glmath_euler_angles glmath_euler_angles;
	
	
union _glmath_transform_params{
	struct{
		glmath_euler_angles r;
		glmath_vec3 s;
		glmath_vec3 t;
	};
	glmath_real p[9];
};
typedef union _glmath_transform_params glmath_transform_params;

	
#ifdef __cplusplus
	}
#endif
	
#endif