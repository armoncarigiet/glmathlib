///===========================================================================
/*
 *  glmath - OpenGL Mathematics Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file glmath_extensions.h
 *	@date 06.10.2014
 *  @author Armon Carigiet
 *
 *  @brief Handels extensions and precision
 */
//===========================================================================

#ifndef glmathlib_glmath_extensions_h
#define glmathlib_glmath_extensions_h

#if (!defined(GLMATH_SINGLE_PRECISION)) && (!defined(GLMATH_DOUBLE_PRECISION)) && (!defined(GLMATH_HIGH_PRECISION))
	#define GLMATH_SINGLE_PRECISION
#endif

#if defined(GLMATH_SINGLE_PRECISION)
	typedef float glmath_real;
	static inline glmath_real glmath_sqrt(glmath_real x){ return sqrtf(x); }
	static inline glmath_real glmath_sin(glmath_real x){ return sinf(x); }
	static inline glmath_real glmath_cos(glmath_real x){ return cosf(x); }
	static inline glmath_real glmath_tan(glmath_real x){ return tanf(x); }
	static inline glmath_real glmath_asin(glmath_real x){ return asinf(x); }
	static inline glmath_real glmath_acos(glmath_real x){ return acosf(x); }
	static inline glmath_real glmath_atan(glmath_real x){ return atanf(x); }
	static inline glmath_real glmath_atan2(glmath_real x, glmath_real y){ return atan2f(x, y); }
	static inline glmath_real glmath_abs(glmath_real x){ return fabsf(x); }
	static inline glmath_real glmath_pow(glmath_real x, glmath_real y){ return powf(x, y); }	// slow
	#if defined(GLMATH_DOUBLE_PRECISION)
		#undef GLMATH_DOUBLE_PRECISION
		#error "Precision is ambiguous! Please choose only one precision type."
	#elif defined(GLMATH_HIGH_PRECISION)
		#undef GLMATH_HIGH_PRECISION
		#error "Precision is ambiguous! Please choose only one precision type."
	#endif
#elif defined(GLMATH_DOUBLE_PRECISION)
	typedef double glmath_real;
	static inline glmath_real glmath_sqrt(glmath_real x){ return sqrt(x); }
	static inline glmath_real glmath_sin(glmath_real x){ return sin(x); }
	static inline glmath_real glmath_cos(glmath_real x){ return cos(x); }
	static inline glmath_real glmath_tan(glmath_real x){ return tan(x); }
	static inline glmath_real glmath_asin(glmath_real x){ return asin(x); }
	static inline glmath_real glmath_acos(glmath_real x){ return acos(x); }
	static inline glmath_real glmath_atan(glmath_real x){ return atan(x); }
	static inline glmath_real glmath_atan2(glmath_real x, glmath_real y){ return atan2(x, y); }
	static inline glmath_real glmath_abs(glmath_real x){ return fabs(x); }
	static inline glmath_real glmath_pow(glmath_real x, glmath_real y){ return pow(x, y); }
	#ifdef GLMATH_HIGH_PRECISION
		#undef GLMATH_HIGH_PRECISION
		#error "Precision is ambiguous! Please choose only one precision type."
	#endif
#elif defined(GLMATH_HIGH_PRECISION)
	typedef long double glmath_real;
	static inline glmath_real glmath_sqrt(glmath_real x){ return sqrtl(x); }
	static inline glmath_real glmath_sin(glmath_real x){ return sinl(x); }
	static inline glmath_real glmath_cos(glmath_real x){ return cosl(x); }
	static inline glmath_real glmath_tan(glmath_real x){ return tanl(x); }
	static inline glmath_real glmath_asin(glmath_real x){ return asinl(x); }
	static inline glmath_real glmath_acos(glmath_real x){ return acosl(x); }
	static inline glmath_real glmath_atan(glmath_real x){ return atanl(x); }
	static inline glmath_real glmath_atan2(glmath_real x, glmath_real y){ return atan2l(x, y); }
	static inline glmath_real glmath_abs(glmath_real x){ return fabsl(x); }
	static inline glmath_real glmath_pow(glmath_real x, glmath_real y){ return powl(x, y); }
#endif

#if defined(__SSE__)
	#define GLMATH_SSE 1
	#if defined(__SSE3__)
		#define GLMATH_SSE3 1
	#else
		#warning "SSE3 instructions not available."
	#endif
#else
	#warning "SSE instructions not available."
#endif

#if defined(__AVX__)
#define GLMATH_AVX2	1
	#if defined(__AVX2__)
		#define GLMATH_AVX2 1
	#else
		#warning "AVX2 instructions not available."
	#endif
#else
	#warning "AVX instructions not available."
#endif

#include <immintrin.h>

#endif