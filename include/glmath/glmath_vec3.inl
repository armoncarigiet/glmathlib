//===========================================================================
/*
 *  glmath - OpenGL Mathematics Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file glmath_vec3.inl
 *	@date 07.10.2014
 *  @author Armon Carigiet
 *  @see glmath_vec3.h
 */
//===========================================================================

static inline glmath_vec3 glmath_vec3_create(glmath_real x, glmath_real y, glmath_real z){
	glmath_vec3 v={{x,y,z}};
	return v;
}

static inline glmath_vec3 glmath_vec3_neg(glmath_vec3 v){
	glmath_vec3 vr={{
		-v.v[0],
		-v.v[1],
		-v.v[2]
	}};
	return vr;
}

static inline glmath_vec3 glmath_vec3_add(glmath_vec3 v1, glmath_vec3 v2){
	glmath_vec3 v={{
		v1.v[0] + v2.v[0],
		v1.v[1] + v2.v[1],
		v1.v[2] + v2.v[2]
	}};
	return v;
}

static inline glmath_vec3 glmath_vec3_sub(glmath_vec3 v1, glmath_vec3 v2){
	glmath_vec3 v={{
		v1.v[0] - v2.v[0],
		v1.v[1] - v2.v[1],
		v1.v[2] - v2.v[2]
	}};
	return v;
}

static inline glmath_vec3 glmath_vec3_mul(glmath_vec3 v1, glmath_vec3 v2){
	glmath_vec3 v={{
		v1.v[0] * v2.v[0],
		v1.v[1] * v2.v[1],
		v1.v[2] * v2.v[2]
	}};
	return v;
}

static inline glmath_vec3 glmath_vec3_div(glmath_vec3 v1, glmath_vec3 v2){
	glmath_vec3 v={{
		v1.v[0] / v2.v[0],
		v1.v[1] / v2.v[1],
		v1.v[2] / v2.v[2]
	}};
	return v;
}

static inline glmath_vec3 glmath_vec3_add_s(glmath_vec3 v, glmath_real a){
	glmath_vec3 vr={{
		v.v[0] + a,
		v.v[1] + a,
		v.v[2] + a
	}};
	return vr;
}

static inline glmath_vec3 glmath_vec3_sub_s(glmath_vec3 v, glmath_real a){
	glmath_vec3 vr={{
		v.v[0] - a,
		v.v[1] - a,
		v.v[2] - a
	}};
	return vr;
}

static inline glmath_vec3 glmath_vec3_mul_s(glmath_vec3 v, glmath_real a){
	glmath_vec3 vr={{
		v.v[0] * a,
		v.v[1] * a,
		v.v[2] * a
	}};
	return vr;
}

static inline glmath_vec3 glmath_vec3_div_s(glmath_vec3 v, glmath_real a){
	glmath_vec3 vr={{
		v.v[0] / a,
		v.v[1] / a,
		v.v[2] / a
	}};
	return vr;
}

static inline glmath_real glmath_vec3_mag(glmath_vec3 v){
	return glmath_sqrt(v.v[0]*v.v[0] + v.v[1]*v.v[1] + v.v[2]*v.v[2]);
}

static inline glmath_real glmath_vec3_dist(glmath_vec3 v1, glmath_vec3 v2){
	return glmath_vec3_mag(glmath_vec3_sub(v2, v1));
}

static inline glmath_vec3 glmath_vec3_norm(glmath_vec3 v){
	return glmath_vec3_div_s(v, glmath_vec3_mag(v));
}

static inline glmath_real glmath_vec3_dot(glmath_vec3 v1, glmath_vec3 v2){
	return v1.v[0]*v2.v[0] + v1.v[1]*v2.v[1] + v1.v[2]*v2.v[2];
}

static inline glmath_vec3 glmath_vec3_cross(glmath_vec3 v1, glmath_vec3 v2){
	glmath_vec3 v={{
		v1.v[1]*v2.v[2] - v1.v[2]*v2.v[1],
		v1.v[2]*v2.v[0] - v1.v[0]*v2.v[2],
		v1.v[0]*v2.v[1] - v1.v[1]*v2.v[0]
	}};
	return v;
}

static inline glmath_vec3 glmath_vec3_max(glmath_vec3 v1, glmath_vec3 v2){
	glmath_vec3 v;
	if(v1.v[0]<v2.v[0]) {
		v.v[0]=v2.v[0];
	}else{
		v.v[0]=v1.v[0];
	}
	if(v1.v[1]<v2.v[1]) {
		v.v[1]=v2.v[1];
	}else{
		v.v[1]=v1.v[1];
	}
	if(v1.v[2]<v2.v[2]) {
		v.v[2]=v2.v[2];
	}else{
		v.v[2]=v1.v[2];
	}
	return v;
}

static inline glmath_vec3 glmath_vec3_min(glmath_vec3 v1, glmath_vec3 v2){
	glmath_vec3 v;
	if(v1.v[0]<v2.v[0]){
		v.v[0]=v1.v[0];
	}else{
		v.v[0]=v2.v[0];
	}
	if(v1.v[1]<v2.v[1]){
		v.v[1]=v1.v[1];
	}else{
		v.v[1]=v2.v[1];
	}
	if(v1.v[2]<v2.v[2]){
		v.v[2]=v1.v[2];
	}else{
		v.v[2]=v2.v[2];
	}
	return v;
}

static inline glmath_vec3 glmath_vec3_lerp(glmath_vec3 sv, glmath_vec3 ev, glmath_real a){
	glmath_vec3 v={{
		sv.v[0] + ((ev.v[0] - sv.v[0])*a),
		sv.v[1] + ((ev.v[1] - sv.v[1])*a),
		sv.v[2] + ((ev.v[2] - sv.v[2])*a)
	}};
    return v;
}

static inline glmath_vec3 glmath_vec3_proj(glmath_vec3 v, glmath_vec3 pv){
	glmath_real s = glmath_vec3_dot(pv, v) /  glmath_vec3_dot(pv, pv);
    glmath_vec3 vr = glmath_vec3_mul_s(pv, s);
    return vr;
}