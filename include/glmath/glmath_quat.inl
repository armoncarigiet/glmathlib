//===========================================================================
/*
 *  glmath - OpenGL Mathematics Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//** 
 *
 *	@file glmath_quat.inl
 *	@date 23.01.15.
 *  @author Armon Carigiet
 *  @see glmath_quat.h
 */
//===========================================================================


static inline glmath_quat glmath_quat_create_identity(){
	glmath_quat q={{
		1,
		0,
		0,
		0
	}};
	return q;
}

static inline glmath_quat glmath_quat_create(glmath_real w, glmath_real x, glmath_real y, glmath_real z){
	glmath_quat q={{
		w,
		x,
		y,
		z
	}};
	return q;
}

static inline glmath_quat glmath_quat_create_vec3(glmath_real w, glmath_vec3 v){
	glmath_quat q={{
		w,
		v.v[0],
		v.v[1],
		v.v[2]
	}};
	return q;
}

static inline glmath_quat glmath_quat_create_mat3(glmath_mat3 m){
	glmath_real w, x, y, z;
	
    glmath_real t=m.m[0]+m.m[4]+m.m[8];
    
	
	
    if(t<0){
        glmath_real s=sqrtf(t+1)*2;
        w = s/4;
        x = (m.m[7] - m.m[5])/s;
        y = (m.m[2] - m.m[6])/s;
        z = (m.m[3] - m.m[1])/s;
    }else if((m.m[0] > m.m[4]) && (m.m[0] > m.m[8])){
        glmath_real s=sqrtf(1.0 + m.m[0] - m.m[4] - m.m[8])*2;
        w = (m.m[7] - m.m[5])/s;
        x = s/4;
        y = (m.m[3] + m.m[1])/s;
        z = (m.m[2] + m.m[6])/s;
    }else if(m.m[4] > m.m[8]){
        glmath_real s=sqrtf(1.0 - m.m[0] + m.m[4] - m.m[8])*2;
        w = (m.m[2] - m.m[6])/s;
        x = (m.m[1] - m.m[3])/s;
        y = s/4;
        z = (m.m[5] - m.m[7])/s;
    }else{
        glmath_real s=sqrtf(1.0 - m.m[0] - m.m[4] + m.m[8])*2;
        w = (m.m[3] - m.m[1])/s;
        x = (m.m[2] + m.m[6])/s;
        y = (m.m[5] - m.m[7])/s;
        z = s/4;
    }
//    
//	if (w!=0) {
//		x=(m.m[7]-m.m[5])/(4*w);
//		y=(m.m[6]-m.m[2])/(4*w);
//		z=(m.m[3]-m.m[1])/(4*w);
//	}else{
//		x=NAN;
//		y=NAN;
//		z=NAN;
//	}
	glmath_quat q={{
		w,
		x,
		y,
		z
	}};
	return q;
}

static inline glmath_quat glmath_quat_create_mat4(glmath_mat4 m){
	glmath_real w=0, x=0, y=0, z=0;
	
    glmath_real t=m.m[0]+m.m[5]+m.m[10];
    
    if(t<0){
        glmath_real s=sqrtf(t+1)*2;
        w = s/4;
        x = (m.m[9] - m.m[6])/s;
        y = (m.m[2] - m.m[8])/s;
        z = (m.m[4] - m.m[1])/s;
    }else if((m.m[0] > m.m[5]) && (m.m[0] > m.m[10])){
        glmath_real s=sqrtf(1.0 + m.m[0] - m.m[5] - m.m[10])*2;
        w = (m.m[9] - m.m[6])/s;
        x = s/4;
        y = (m.m[4] + m.m[1])/s;
        z = (m.m[2] + m.m[8])/s;
    }else if(m.m[5] > m.m[10]){
        glmath_real s=sqrtf(1.0 - m.m[0] + m.m[5] - m.m[10])*2;
        w = (m.m[2] - m.m[8])/s;
        x = (m.m[1] - m.m[4])/s;
        y = s/4;
        z = (m.m[6] - m.m[9])/s;
    }else{
        glmath_real s=sqrtf(1.0 - m.m[0] - m.m[5] + m.m[10])*2;
        w = (m.m[4] - m.m[1])/s;
        x = (m.m[2] + m.m[8])/s;
        y = (m.m[6] - m.m[9])/s;
        z = s/4;
    }
    
    

    
    
//	w=glmath_sqrt(1+m.m[0]+m.m[5]+m.m[10])/2;
//	
//	if (w!=0) {
//		x=(m.m[6]-m.m[9])/(4*w);
//		y=(m.m[8]-m.m[2])/(4*w);
//		z=(m.m[1]-m.m[4])/(4*w);
//	}else{
//		x=NAN;
//		y=NAN;
//		z=NAN;
//	}
	glmath_quat q={{
		w,
		x,
		y,
		z
	}};
	return q;

}

static inline glmath_quat glmath_quat_create_rotation(glmath_real rad, glmath_vec3 v){
	glmath_real delta = rad/2;
	glmath_real sind = glmath_sin(delta);
	glmath_quat q={{
		glmath_cos(delta),
		sind*v.v[0],
		sind*v.v[1],
		sind*v.v[2]
	}};
	return q;
}

static inline glmath_quat glmath_quat_create_xrotation(glmath_real rad){
	glmath_real delta = rad/2;
	glmath_quat q={{
		glmath_cos(delta),
		glmath_sin(delta),
		0,
		0
	}};
	return q;
}

static inline glmath_quat glmath_quat_create_yrotation(glmath_real rad){
	glmath_real delta = rad/2;
	glmath_quat q={{
		glmath_cos(delta),
		0,
		glmath_sin(delta),
		0
	}};
	return q;
}

static inline glmath_quat glmath_quat_create_zrotation(glmath_real rad){
	glmath_real delta = rad/2;
	glmath_quat q={{
		glmath_cos(delta),
		0,
		0,
		glmath_sin(delta)
	}};
	return q;
}

static inline glmath_quat glmath_quat_create_rotation_euler_angles(glmath_euler_angles a){
	glmath_real cosx=glmath_cos(a.r[0]);
	glmath_real cosy=glmath_cos(a.r[1]);
	glmath_real cosz=glmath_cos(a.r[2]);
	glmath_real sinx=glmath_sin(a.r[0]);
	glmath_real siny=glmath_sin(a.r[1]);
	glmath_real sinz=glmath_sin(a.r[2]);
	glmath_quat q={{
		cosx*cosy*cosz+sinx*siny*sinz,
		sinx*cosy*cosz-cosx*siny*sinz,
		cosx*siny*cosz+sinx*cosy*sinz,
		cosx*cosy*sinz-sinx*siny*cosz
	}};
	return q;
}

static inline glmath_vec3 glmath_quat_rotate_vec3(glmath_vec3 v, glmath_quat q){
	// impementation from glm (opengl mathematics library)
    glmath_quat qu=glmath_quat_invert(q);
    
    glmath_vec3 quat=glmath_vec3_create(qu.x, qu.y, qu.z);
    glmath_vec3 uv=glmath_vec3_cross(quat, v);
    glmath_vec3 uuv=glmath_vec3_cross(quat, uv);
    
    glmath_vec3 t=glmath_vec3_add(glmath_vec3_mul_s(uv, qu.w), uuv);
    
    return glmath_vec3_add(v, glmath_vec3_mul_s(t, 2.0f));
    
    
    
//    q=glmath_quat_norm(q);
//	glmath_quat vec={{
//		0,
//		v.v[0],
//		v.v[1],
//		v.v[2]
//	}};
//    glmath_quat q2=glmath_quat_mul(glmath_quat_mul(q, vec), glmath_quat_invert(q));
//    //glmath_quat q2=glmath_quat_mul(q, vec);
//	glmath_vec3 r={{
//		q2.q[1],
//		q2.q[2],
//		q2.q[3]
//	}};
//	return r;
}

static inline glmath_vec4 glmath_quat_rotate_vec4(glmath_vec4 v, glmath_quat q){
	// impementation from glm (opengl mathematics library)
    glmath_quat qu=glmath_quat_invert(q);
    
    glmath_vec4 quat=glmath_vec4_create(qu.x, qu.y, qu.z, 0);
    glmath_vec4 uv=glmath_vec4_cross(quat, v);
    glmath_vec4 uuv=glmath_vec4_cross(quat, uv);
    
    glmath_vec4 t=glmath_vec4_add(glmath_vec4_mul_s(uv, qu.w), uuv);
    
    return glmath_vec4_add(v, glmath_vec4_mul_s(t, 2.0f));
    
    
    
//    q=glmath_quat_norm(q);
//	glmath_quat vec={{
//		0,
//		v.v[0],
//		v.v[1],
//		v.v[2]
//	}};
//	glmath_quat q2=glmath_quat_mul(glmath_quat_mul(q, vec), glmath_quat_invert(q));
//	glmath_vec4 r={{
//		q2.q[1],
//		q2.q[2],
//		q2.q[3],
//		v.v[3]
//	}};
//	return r;
}

static inline glmath_real glmath_quat_angle(glmath_quat q){
	return 2*glmath_acos(q.q[0]);
}

static inline glmath_vec3 glmath_quat_axis(glmath_quat q){
	glmath_real delta=glmath_acos(q.q[0]);
	glmath_real sind=glmath_sin(delta);
	glmath_vec3 v={{
		q.q[1]/sind,
		q.q[2]/sind,
		q.q[3]/sind,
	}};
	return v;
}

static inline glmath_real glmath_quat_mag(glmath_quat q){
	return sqrtf(q.q[0]*q.q[0] + q.q[1]*q.q[1] + q.q[2]*q.q[2] + q.q[3]*q.q[3]);
}

static inline glmath_quat glmath_quat_add(glmath_quat q1, glmath_quat q2){
#ifdef GLMATH_SSE3_INS
	glmath_quat rq;
#ifdef GLMATH_SINGLE_PRECISION
	_mm_store_ps(&rq.q[0], _mm_load_ps(&q1.q[0]) + _mm_load_ps(&q2.q[0]));
#endif
#ifdef GLMATH_DOUBLE_PRECISION
	_mm_store_pd(&rq.q[0], _mm_load_pd(&q1.q[0]) + _mm_load_pd(&q2.q[0]));
	_mm_store_pd(&rq.q[2], _mm_load_pd(&q1.q[2]) + _mm_load_pd(&q2.q[2]));
#endif
#else
	glmath_quat rq={{
		q1.q[0] + q2.q[0],
		q1.q[1] + q2.q[1],
		q1.q[2] + q2.q[2],
		q1.q[3] + q2.q[3]
	}};
#endif
	return rq;
}

static inline glmath_quat glmath_quat_sub(glmath_quat q1, glmath_quat q2){
#ifdef GLMATH_SSE3_INS
	glmath_quat rq;
#ifdef GLMATH_SINGLE_PRECISION
	_mm_store_ps(&rq.q[0], _mm_load_ps(&q1.q[0]) - _mm_load_ps(&q2.q[0]));
#endif
#ifdef GLMATH_DOUBLE_PRECISION
	_mm_store_pd(&rq.q[0], _mm_load_pd(&q1.q[0]) - _mm_load_pd(&q2.q[0]));
	_mm_store_pd(&rq.q[2], _mm_load_pd(&q1.q[2]) - _mm_load_pd(&q2.q[2]));
#endif
#else
	glmath_quat rq={{
		q1.q[0] - q2.q[0],
		q1.q[1] - q2.q[1],
		q1.q[2] - q2.q[2],
		q1.q[3] - q2.q[3]
	}};
#endif
	return rq;
}

static inline glmath_quat glmath_quat_mul(glmath_quat q1, glmath_quat q2){
//#ifdef GLMATH_SSE3_INS
//	glmath_quat rq;
//#ifdef GLMATH_SINGLE_PRECISION
//	const __m128 a=_mm_load_ps(&q1.q[0]);
//	const __m128 b=_mm_load_ps(&q2.q[0]);
//	
//	const __m128 r = _mm_shuffle_ps(a, a, _MM_SHUFFLE(0, 0, 0, 0))*_mm_shuffle_ps(b, b, _MM_SHUFFLE(3, 2, 1, 0))
//	- _mm_shuffle_ps(a, a, _MM_SHUFFLE(1, 1, 1, 1))*_mm_shuffle_ps(b, b, _MM_SHUFFLE(2, 3, 0, 1))
//	- _mm_shuffle_ps(a, a, _MM_SHUFFLE(2, 2, 2, 2))*_mm_shuffle_ps(b, b, _MM_SHUFFLE(1, 0, 3, 2))
//	- _mm_shuffle_ps(a, a, _MM_SHUFFLE(3, 3, 3, 3))*_mm_shuffle_ps(b, b, _MM_SHUFFLE(0, 1, 2, 3));
//	
//	_mm_store_ps(&rq.q[0], r);
//#endif
//#ifdef GLMATH_DOUBLE_PRECISION
//	const __m128d a1=_mm_load_pd(&q1.q[0]);
//	const __m128d a2=_mm_load_pd(&q1.q[2]);
//	const __m128d b1=_mm_load_pd(&q2.q[0]);
//	const __m128d b2=_mm_load_pd(&q2.q[2]);
//	
//	const __m128d c1=_mm_shuffle_pd(a1, a1, _MM_SHUFFLE2(0, 0));
//	const __m128d c2=_mm_shuffle_pd(a1, a1, _MM_SHUFFLE2(1, 1));
//	const __m128d c3=_mm_shuffle_pd(a2, a2, _MM_SHUFFLE2(0, 0));
//	const __m128d c4=_mm_shuffle_pd(a2, a2, _MM_SHUFFLE2(1, 1));
//	
//	const __m128d d1=_mm_shuffle_pd(b1, b1, _MM_SHUFFLE2(1, 0));
//	const __m128d d2=_mm_shuffle_pd(b1, b1, _MM_SHUFFLE2(0, 1));
//	const __m128d d3=_mm_shuffle_pd(b2, b2, _MM_SHUFFLE2(1, 0));
//	const __m128d d4=_mm_shuffle_pd(b2, b2, _MM_SHUFFLE2(0, 1));
//	
//	_mm_store_pd(&rq.q[0], c1*d1 - c2*d2 - c3*d3 - c4*d4);
//	_mm_store_pd(&rq.q[2], c1*d3 - c2*d4 - c3*d1 - c4*d2);
//#endif
//#else
	glmath_quat rq={{
		q1.q[0]*q2.q[0] - q1.q[1]*q2.q[1] - q1.q[2]*q2.q[2] - q1.q[3]*q2.q[3],
		q1.q[0]*q2.q[1] + q1.q[1]*q2.q[0] + q1.q[2]*q2.q[3] - q1.q[3]*q2.q[2],
		q1.q[0]*q2.q[2] - q1.q[1]*q2.q[3] + q1.q[2]*q2.q[0] + q1.q[3]*q2.q[1],
		q1.q[0]*q2.q[3] + q1.q[1]*q2.q[2] - q1.q[2]*q2.q[1] + q1.q[3]*q2.q[0]
	}};
//    glmath_quat rq={{
//        q1.q[0]*q2.q[0] - q1.q[1]*q2.q[1] - q1.q[2]*q2.q[2] - q1.q[3]*q2.q[3],
//        q1.q[0]*q2.q[1] - q1.q[1]*q2.q[0] - q1.q[2]*q2.q[3] - q1.q[3]*q2.q[2],
//        q1.q[0]*q2.q[2] - q1.q[1]*q2.q[3] - q1.q[2]*q2.q[0] - q1.q[3]*q2.q[1],
//        q1.q[0]*q2.q[3] - q1.q[1]*q2.q[2] - q1.q[2]*q2.q[1] - q1.q[3]*q2.q[0]
//    }};
    
//#endif
	return rq;
}

static inline glmath_real glmath_quat_dot(glmath_quat q1, glmath_quat q2){
	glmath_real f;
#ifdef GLMATH_SSE3_INS
#ifdef GLMATH_SINGLE_PRECISION
	const __m128 product = _mm_load_ps(&q1.q[0]) * _mm_load_ps(&q2.q[0]);
	const __m128 halfsum = _mm_hadd_ps(product, product);
	f = _mm_cvtss_f32(_mm_hadd_ps(halfsum, halfsum));
#endif
#ifdef GLMATH_DOUBLE_PRECISION
	const __m128d product1 = _mm_load_pd(&q1.q[0]) * _mm_load_pd(&q2.q[0]);
	const __m128d product2 = _mm_load_pd(&q1.q[2]) * _mm_load_pd(&q2.q[2]);
	const __m128d halfsum = _mm_hadd_pd(product1, product2);
	f = _mm_cvtsd_f64(_mm_hadd_pd(halfsum, halfsum));
#endif
#else
	f=q1.q[0]*q2.q[0] + q1.q[1]*q2.q[1] + q1.q[2]*q2.q[2] + q1.q[3]*q2.q[3];
#endif
	
	return f;
}

static inline glmath_quat glmath_quat_cross(glmath_quat q1, glmath_quat q2){
#ifdef GLMATH_SSE3_INS
	glmath_quat q;
#ifdef GLMATH_SINGLE_PRECISION
	const __m128 va = _mm_load_ps(&q1.q[0]);
	const __m128 vb = _mm_load_ps(&q2.q[0]);
	__m128 a1 = _mm_shuffle_ps(va, va, _MM_SHUFFLE(2, 1, 3, 0));
	__m128 a2 = _mm_shuffle_ps(vb, vb, _MM_SHUFFLE(2, 1, 3, 0));
	__m128 b1 = _mm_shuffle_ps(va, va, _MM_SHUFFLE(1, 3, 2, 0));
	__m128 b2 = _mm_shuffle_ps(vb, vb, _MM_SHUFFLE(1, 3, 2, 0));
	
	b1 = _mm_mul_ps(b1, a2);
	b2 = _mm_mul_ps(b2, a1);
	b1 = _mm_sub_ps(b1, b2);
	
	_mm_store_ps(&q.q[0], b1);
#endif
#ifdef GLMATH_DOUBLE_PRECISION
	//TODO
	
	const __m128d a1=_mm_load_pd(&q1.q[0]);
	const __m128d b1=_mm_load_pd(&q1.q[2]);
	const __m128d a2=_mm_load_pd(&q2.q[0]);
	const __m128d b2=_mm_load_pd(&q2.q[2]);
	
	
	const __m128d k1=_mm_shuffle_pd(a1, b1, _MM_SHUFFLE2(0, 0));
	const __m128d k2=_mm_shuffle_pd(b1, a1, _MM_SHUFFLE2(1, 1));
	const __m128d j1=_mm_shuffle_pd(a2, b2, _MM_SHUFFLE2(1, 0));
	const __m128d j2=_mm_shuffle_pd(a2, b2, _MM_SHUFFLE2(0, 1));
	
	const __m128d k=_mm_mul_pd(k1, j1);
	const __m128d j=_mm_mul_pd(k2, j2);
	
	
	const __m128d p1=_mm_shuffle_pd(a1, b1, _MM_SHUFFLE2(1, 0));
	const __m128d p2=_mm_shuffle_pd(a1, b1, _MM_SHUFFLE2(0, 1));
	const __m128d qd1=_mm_shuffle_pd(a2, b2, _MM_SHUFFLE2(0, 0));
	const __m128d qd2=_mm_shuffle_pd(b2, a2, _MM_SHUFFLE2(1, 1));
	const __m128d p=_mm_mul_pd(p1, qd1);
	const __m128d qd=_mm_mul_pd(p2, qd2);
	
	_mm_store_pd(&q.q[0], _mm_sub_pd(k, p));
	_mm_store_pd(&q.q[2], _mm_sub_pd(j, qd));
	
	//v.v[3]=0.0f;
#endif
#else
	glmath_quat q={{
		0,
		q1.q[2]*q2.q[3] + q1.q[3]*q2.q[2],
		q1.q[3]*q2.q[1] + q1.q[1]*q2.q[3],
		q1.q[1]*q2.q[2] + q1.q[2]*q2.q[1],
	}};
#endif
	return q;
}

static inline glmath_quat glmath_quat_conj(glmath_quat q){
	glmath_quat rq={{
		q.q[0],
		-q.q[1],
		-q.q[2],
		-q.q[3]
	}};
	return rq;
}

static inline glmath_quat glmath_quat_invert(glmath_quat q){
	glmath_real s=q.q[0]*q.q[0] + q.q[1]*q.q[1] + q.q[2]*q.q[2] + q.q[3]*q.q[3];
	glmath_quat qr={{
		q.q[0]/s,
		-q.q[1]/s,
		-q.q[2]/s,
		-q.q[3]/s
	}};
	return qr;
}

static inline glmath_quat glmath_quat_norm(glmath_quat q){
	glmath_real mag=glmath_quat_mag(q);
	glmath_quat qr={{
		q.q[0]/mag,
		q.q[1]/mag,
		q.q[2]/mag,
		q.q[3]/mag
	}};
	return qr;
}

static inline glmath_quat glmath_quat_slerp(glmath_quat q0, glmath_quat q1, glmath_real t){
	glmath_quat qtemp=glmath_quat_mul(glmath_quat_invert(q0), q1);
	glmath_real omega=glmath_acos(qtemp.q[0]);
	glmath_real sinomega=glmath_sin(omega);
	glmath_vec3 vec={{
		qtemp.q[1]/sinomega,
		qtemp.q[2]/sinomega,
		qtemp.q[3]/sinomega
	}};
	glmath_real sinomegat=glmath_sin(t*omega);
	glmath_quat q={{
		glmath_cos(omega),
		vec.v[0]*sinomegat,
		vec.v[1]*sinomegat,
		vec.v[2]*sinomegat
	}};
	return q;
}