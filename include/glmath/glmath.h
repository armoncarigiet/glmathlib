//===========================================================================
/*
 *  glmath - OpenGL Mathematics Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file glmath.h
 *	@date 03.10.2014
 *  @author Armon Carigiet
 *
 *  @brief Main include file.
 */
//===========================================================================

#ifndef glmathlib_glmath_h
#define glmathlib_glmath_h

#include "glmath_extensions.h"
#include "glmath_types.h"
#include "glmath_constants.h"
#include "glmath_vec2.h"
#include "glmath_vec3.h"
#include "glmath_vec4.h"
#include "glmath_mat2.h"
#include "glmath_mat3.h"
#include "glmath_mat3.h"
#include "glmath_quat.h"

#endif