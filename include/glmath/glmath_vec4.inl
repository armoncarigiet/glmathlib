//===========================================================================
/*
 *  glmath - OpenGL Mathematics Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file glmath_vec4.inl
 *	@date 10.10.2014
 *  @author Armon Carigiet
 *  @see glmath_vec4.h
 */
//===========================================================================

// AVX not tested yet!


static inline glmath_vec4 glmath_vec4_create(glmath_real x, glmath_real y, glmath_real z, glmath_real w){
	glmath_vec4 v={{x,y,z,w}};
	return v;
}

static inline glmath_vec4 glmath_vec4_neg(glmath_vec4 v){
	glmath_vec4 vr={{
		-v.v[0],
		-v.v[1],
		-v.v[2],
		-v.v[3]
	}};
	return vr;
}

static inline glmath_vec4 glmath_vec4_add(glmath_vec4 v1, glmath_vec4 v2){
	glmath_vec4 v;
#if defined(GLMATH_SINGLE_PRECISION) && defined(GLMATH_SSE)
	_mm_store_ps(&v.v[0], _mm_add_ps(_mm_load_ps(&v1.v[0]), _mm_load_ps(&v2.v[0])));
#elif defined(GLMATH_DOUBLE_PRECISION) && defined(GLMATH_AVX)
	_mm256_store_pd(&v.v[0], _mm256_add_pd(_mm256_load_pd(&v1.v[0]), _mm256_load_pd(&v2.v[0])));
#elif defined(GLMATH_DOUBLE_PRECISION) && defined(GLMATH_SSE)
	_mm_store_pd(&v.v[0], _mm_add_pd(_mm_load_pd(&v1.v[0]), _mm_load_pd(&v2.v[0])));
	_mm_store_pd(&v.v[2], _mm_add_pd(_mm_load_pd(&v1.v[2]), _mm_load_pd(&v2.v[2])));
#else
	v.v[0]=v1.v[0]+v2.v[0];
	v.v[1]=v1.v[1]+v2.v[1];
	v.v[2]=v1.v[2]+v2.v[2];
	v.v[3]=v1.v[3]+v2.v[3];
#endif
	return v;
}

static inline glmath_vec4 glmath_vec4_sub(glmath_vec4 v1, glmath_vec4 v2){
	glmath_vec4 v;
#if defined(GLMATH_SINGLE_PRECISION) && defined(GLMATH_SSE)
	_mm_store_ps(&v.v[0], _mm_sub_ps(_mm_load_ps(&v1.v[0]), _mm_load_ps(&v2.v[0])));
#elif defined(GLMATH_DOUBLE_PRECISION) && defined(GLMATH_AVX)
	_mm256_store_pd(&v.v[0], _mm256_sub_pd(_mm256_load_pd(&v1.v[0]), _mm256_load_pd(&v2.v[0])));
#elif defined(GLMATH_DOUBLE_PRECISION) && defined(GLMATH_SSE)
	_mm_store_pd(&v.v[0], _mm_sub_pd(_mm_load_pd(&v1.v[0]), _mm_load_pd(&v2.v[0])));
	_mm_store_pd(&v.v[2], _mm_sub_pd(_mm_load_pd(&v1.v[2]), _mm_load_pd(&v2.v[2])));
#else
	v.v[0]=v1.v[0]-v2.v[0];
	v.v[1]=v1.v[1]-v2.v[1];
	v.v[2]=v1.v[2]-v2.v[2];
	v.v[3]=v1.v[3]-v2.v[3];
#endif
	return v;
}

static inline glmath_vec4 glmath_vec4_mul(glmath_vec4 v1, glmath_vec4 v2){
	glmath_vec4 v;
#if defined(GLMATH_SINGLE_PRECISION) && defined(GLMATH_SSE)
	_mm_store_ps(&v.v[0], _mm_mul_ps(_mm_load_ps(&v1.v[0]), _mm_load_ps(&v2.v[0])));
#elif defined(GLMATH_DOUBLE_PRECISION) && defined(GLMATH_AVX)
	_mm256_store_pd(&v.v[0], _mm256_mul_pd(_mm256_load_pd(&v1.v[0]), _mm256_load_pd(&v2.v[0])));
#elif defined(GLMATH_DOUBLE_PRECISION) && defined(GLMATH_SSE)
	_mm_store_pd(&v.v[0], _mm_mul_pd(_mm_load_pd(&v1.v[0]), _mm_load_pd(&v2.v[0])));
	_mm_store_pd(&v.v[2], _mm_mul_pd(_mm_load_pd(&v1.v[2]), _mm_load_pd(&v2.v[2])));
#else
	v.v[0]=v1.v[0]*v2.v[0];
	v.v[1]=v1.v[1]*v2.v[1];
	v.v[2]=v1.v[2]*v2.v[2];
	v.v[3]=v1.v[3]*v2.v[3];
#endif
	return v;
}

static inline glmath_vec4 glmath_vec4_div(glmath_vec4 v1, glmath_vec4 v2){
	glmath_vec4 v;
#if defined(GLMATH_SINGLE_PRECISION) && defined(GLMATH_SSE)
	_mm_store_ps(&v.v[0], _mm_div_ps(_mm_load_ps(&v1.v[0]), _mm_load_ps(&v2.v[0])));
#elif defined(GLMATH_DOUBLE_PRECISION) && defined(GLMATH_AVX)
	_mm256_store_pd(&v.v[0], _mm256_div_pd(_mm256_load_pd(&v1.v[0]), _mm256_load_pd(&v2.v[0])));
#elif defined(GLMATH_DOUBLE_PRECISION) && defined(GLMATH_SSE)
	_mm_store_pd(&v.v[0], _mm_div_pd(_mm_load_pd(&v1.v[0]), _mm_load_pd(&v2.v[0])));
	_mm_store_pd(&v.v[2], _mm_div_pd(_mm_load_pd(&v1.v[2]), _mm_load_pd(&v2.v[2])));
#else
	v.v[0]=v1.v[0]/v2.v[0];
	v.v[1]=v1.v[1]/v2.v[1];
	v.v[2]=v1.v[2]/v2.v[2];
	v.v[3]=v1.v[3]/v2.v[3];
#endif
	return v;
}

static inline glmath_vec4 glmath_vec4_add_s(glmath_vec4 v1, glmath_real a){
	glmath_vec4 v;
#if defined(GLMATH_SINGLE_PRECISION) && defined(GLMATH_SSE)
	_mm_store_ps(&v.v[0], _mm_add_ps(_mm_load_ps(&v1.v[0]), _mm_load1_ps(&a)));
#elif defined(GLMATH_DOUBLE_PRECISION) && defined(GLMATH_AVX)
	_mm256_store_pd(&v.v[0], _mm256_add_pd(_mm256_load_pd(&v1.v[0]), _mm256_broadcast_sd(&a)));
#elif defined(GLMATH_DOUBLE_PRECISION) && defined(GLMATH_SSE)
	_mm_store_pd(&v.v[0], _mm_add_pd(_mm_load_pd(&v1.v[0]), _mm_load1_pd(&a)));
	_mm_store_pd(&v.v[2], _mm_add_pd(_mm_load_pd(&v1.v[2]), _mm_load1_pd(&a)));
#else
	v.v[0]=v1.v[0]+a;
	v.v[1]=v1.v[1]+a;
	v.v[2]=v1.v[2]+a;
	v.v[3]=v1.v[3]+a;
#endif
	return v;
}

static inline glmath_vec4 glmath_vec4_sub_s(glmath_vec4 v1, glmath_real a){
	glmath_vec4 v;
#if defined(GLMATH_SINGLE_PRECISION) && defined(GLMATH_SSE)
	_mm_store_ps(&v.v[0], _mm_sub_ps(_mm_load_ps(&v1.v[0]), _mm_load1_ps(&a)));
#elif defined(GLMATH_DOUBLE_PRECISION) && defined(GLMATH_AVX)
	_mm256_store_pd(&v.v[0], _mm256_sub_pd(_mm256_load_pd(&v1.v[0]), _mm256_broadcast_sd(&a)));
#elif defined(GLMATH_DOUBLE_PRECISION) && defined(GLMATH_SSE)
	_mm_store_pd(&v.v[0], _mm_sub_pd(_mm_load_pd(&v1.v[0]), _mm_load1_pd(&a)));
	_mm_store_pd(&v.v[2], _mm_sub_pd(_mm_load_pd(&v1.v[2]), _mm_load1_pd(&a)));
#else
	v.v[0]=v1.v[0]-a;
	v.v[1]=v1.v[1]-a;
	v.v[2]=v1.v[2]-a;
	v.v[3]=v1.v[3]-a;
#endif
	return v;
}

static inline glmath_vec4 glmath_vec4_mul_s(glmath_vec4 v1, glmath_real a){
	glmath_vec4 v;
#if defined(GLMATH_SINGLE_PRECISION) && defined(GLMATH_SSE)
	_mm_store_ps(&v.v[0], _mm_mul_ps(_mm_load_ps(&v1.v[0]), _mm_load1_ps(&a)));
#elif defined(GLMATH_DOUBLE_PRECISION) && defined(GLMATH_AVX)
	_mm256_store_pd(&v.v[0], _mm256_mul_pd(_mm256_load_pd(&v1.v[0]), _mm256_broadcast_sd(&a)));
#elif defined(GLMATH_DOUBLE_PRECISION) && defined(GLMATH_SSE)
	_mm_store_pd(&v.v[0], _mm_mul_pd(_mm_load_pd(&v1.v[0]), _mm_load1_pd(&a)));
	_mm_store_pd(&v.v[2], _mm_mul_pd(_mm_load_pd(&v1.v[2]), _mm_load1_pd(&a)));
#else
	v.v[0]=v1.v[0]*a;
	v.v[1]=v1.v[1]*a;
	v.v[2]=v1.v[2]*a;
	v.v[3]=v1.v[3]*a;
#endif
	return v;
}

static inline glmath_vec4 glmath_vec4_div_s(glmath_vec4 v1, glmath_real a){
	glmath_vec4 v;
#if defined(GLMATH_SINGLE_PRECISION) && defined(GLMATH_SSE)
	_mm_store_ps(&v.v[0], _mm_div_ps(_mm_load_ps(&v1.v[0]), _mm_load1_ps(&a)));
#elif defined(GLMATH_DOUBLE_PRECISION) && defined(GLMATH_AVX)
	_mm256_store_pd(&v.v[0], _mm256_div_pd(_mm256_load_pd(&v1.v[0]), _mm256_broadcast_sd(&a)));
#elif defined(GLMATH_DOUBLE_PRECISION) && defined(GLMATH_SSE)
	_mm_store_pd(&v.v[0], _mm_div_pd(_mm_load_pd(&v1.v[0]), _mm_load1_pd(&a)));
	_mm_store_pd(&v.v[2], _mm_div_pd(_mm_load_pd(&v1.v[2]), _mm_load1_pd(&a)));
#else
	v.v[0]=v1.v[0]+a;
	v.v[1]=v1.v[1]+a;
	v.v[2]=v1.v[2]+a;
	v.v[3]=v1.v[3]+a;
#endif
	return v;
}

static inline glmath_real glmath_vec4_mag(glmath_vec4 v){
	glmath_real r;
#if defined(GLMATH_SINGLE_PRECISION) && defined(GLMATH_SSE3)
	const __m128 vec=_mm_load_ps(&v.v[0]);
	const __m128 m=_mm_mul_ps(vec, vec);
	const __m128 hsum=_mm_hadd_ps(m, m);
	r=glmath_sqrt(_mm_cvtss_f32(_mm_hadd_ps(hsum, hsum)));
#elif defined(GLMATH_DOUBLE_PRECISION) && defined(GLMATH_AVX)
	const __m256d vec=_mm256_load_pd(&v.v[0]);
	const __m256d m=_mm256_mul_pd(vec, vec);
	const __m256d hsum=_mm256_hadd_pd(m, m);
	r=glmath_sqrt(_mm_cvtss_f64(_mm_256_extractf_pd(_mm256_hadd_pd(hsum, hsum), 0)));
#elif defined(GLMATH_DOUBLE_PRECISION) && defined(GLMATH_SSE3)
	const __m128d vec1=_mm_load_pd(&v.v[0]);
	const __m128d vec2=_mm_load_pd(&v.v[2]);
	const __m128d m1=_mm_mul_pd(vec1, vec1);
	const __m128d m2=_mm_mul_pd(vec2, vec2);
	const __m128d hsum=_mm_hadd_pd(m1, m2);
	r=glmath_sqrt(_mm_cvtss_f64(_mm_hadd_pd(hsum, hsum)));
#else
	r=glmath_sqrt(v.v[0]*v.v[0] + v.v[1]*v.v[1] + v.v[2]*v.v[2] +v.v[3]*v.v[3]);
#endif
	return r;
}

static inline glmath_real glmath_vec4_dist(glmath_vec4 v1, glmath_vec4 v2){
	return glmath_vec4_mag(glmath_vec4_sub(v1, v2));
}

static inline glmath_vec4 glmath_vec4_norm(glmath_vec4 v){
	return glmath_vec4_div_s(v, glmath_vec4_mag(v));
}

static inline glmath_real glmath_vec4_dot(glmath_vec4 v1, glmath_vec4 v2){
	glmath_real r;
#if defined(GLMATH_SINGLE_PRECISION) && defined(GLMATH_SSE3)
	const __m128 m=_mm_mul_ps(_mm_load_ps(&v1.v[0]), _mm_load_ps(&v2.v[0]));
	const __m128 hsum=_mm_hadd_ps(m, m);
	r=_mm_cvtss_f32(_mm_hadd_ps(hsum, hsum));
#elif defined(GLMATH_DOUBLE_PRECISION) && defined(GLMATH_AVX)
	const __m256d m=_mm256_mul_pd(_mm256_load_pd(&v1.v[0]), _mm256_load_pd(&v2.v[0]));
	const __m256d hsum=_mm256_hadd_pd(m, m);
	r=_mm_cvtss_f64(_mm_256_extractf_pd(_mm256_hadd_pd(hsum, hsum), 0));
#elif defined(GLMATH_DOUBLE_PRECISION) && defined(GLMATH_SSE3)
	const __m128d m1=_mm_mul_pd(_mm_load_pd(&v1.v[0]), _mm_load_pd(&v2.v[0]));
	const __m128d m2=_mm_mul_pd(_mm_load_pd(&v1.v[2]), _mm_load_pd(&v2.v[2]));
	const __m128d hsum=_mm_hadd_pd(m1, m2);
	r=_mm_cvtss_f64(_mm_hadd_pd(hsum, hsum));
#else
	r=v1.v[0]*v2.v[0] + v1.v[1]*v2.v[1] + v1.v[2]*v2.v[2] +v1.v[3]*v2.v[3];
#endif
	return r;
}

static inline glmath_vec4 glmath_vec4_cross(glmath_vec4 v1, glmath_vec4 v2){
	glmath_vec4 v;
#if defined(GLMATH_SINGLE_PRECISION) && defined(GLMATH_SSE)
	const __m128 va = _mm_load_ps(&v1.v[0]);
	const __m128 vb = _mm_load_ps(&v2.v[0]);
	const __m128 a1 = _mm_shuffle_ps(va, va, _MM_SHUFFLE(3, 1, 0, 2));
	const __m128 a2 = _mm_shuffle_ps(vb, vb, _MM_SHUFFLE(3, 1, 0, 2));
	__m128 b1 = _mm_shuffle_ps(va, va, _MM_SHUFFLE(3, 0, 2, 1));
	__m128 b2 = _mm_shuffle_ps(vb, vb, _MM_SHUFFLE(3, 0, 2, 1));
	
	b1 = _mm_mul_ps(b1, a2);
	b2 = _mm_mul_ps(b2, a1);
	b1 = _mm_sub_ps(b1, b2);
	
	_mm_store_ps(&v.v[0], b1);
	v.v[3]=0.0f;
#elif defined(GLMATH_DOUBLE_PRECISION) && defined(GLMATH_AVX)
	//TODO better implementation (not ideal)
	const __m256d a1 = _mm256_set_pd(v1.v[3], v1.v[0], v1.v[2], v1.v[1]);
	const __m256d a2 = _mm256_set_pd(v1.v[3], v1.v[1], v1.v[0], v1.v[2]);
	const __m256d b1 = _mm256_set_pd(v2.v[3], v2.v[1], v2.v[0], v2.v[2]);
	const __m256d b2 = _mm256_set_pd(v2.v[3], v2.v[0], v2.v[2], v2.v[1]);
	
	a1 = _mm256_mul_pd(a1, b1);
	a2 = _mm256_mul_pd(a2, b2);
	a1 = _mm256_sub_pd(a1, a2);
	
#elif defined(GLMATH_DOUBLE_PRECISION) && defined(GLMATH_SSE3)
	const __m128d a1=_mm_load_pd(&v1.v[0]);
	const __m128d b1=_mm_load_pd(&v1.v[2]);
	const __m128d a2=_mm_load_pd(&v2.v[0]);
	const __m128d b2=_mm_load_pd(&v2.v[2]);
	
	const __m128d k1=_mm_shuffle_pd(a1, b1, _MM_SHUFFLE2(0, 1));
	const __m128d k2=_mm_shuffle_pd(a1, b1, _MM_SHUFFLE2(1, 0));
	const __m128d j1=_mm_shuffle_pd(b2, a2, _MM_SHUFFLE2(0, 0));
	const __m128d j2=_mm_shuffle_pd(a2, b2, _MM_SHUFFLE2(1, 1));
	
	const __m128d k=_mm_mul_pd(k1, j1);
	const __m128d j=_mm_mul_pd(k2, j2);
	
	const __m128d p1=_mm_shuffle_pd(b1, a1, _MM_SHUFFLE2(0, 0));
	const __m128d p2=_mm_shuffle_pd(a1, b1, _MM_SHUFFLE2(1, 1));
	const __m128d q1=_mm_shuffle_pd(a2, b2, _MM_SHUFFLE2(0, 1));
	const __m128d q2=_mm_shuffle_pd(a2, b2, _MM_SHUFFLE2(1, 0));
	
	const __m128d p=_mm_mul_pd(p1, q1);
	const __m128d q=_mm_mul_pd(p2, q2);
	
	_mm_store_pd(&v.v[0], _mm_sub_pd(k, p));
	_mm_store_pd(&v.v[2], _mm_sub_pd(j, q));
#else
	v.v[0]=v1.v[1]*v2.v[2] - v1.v[2]*v2.v[1];
	v.v[1]=v1.v[2]*v2.v[0] - v1.v[0]*v2.v[2];
	v.v[2]=v1.v[0]*v2.v[1] - v1.v[1]*v2.v[0];
	v.v[3]=0.0f;
#endif
	return v;
}