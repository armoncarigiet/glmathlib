//===========================================================================
/*
 *  glmath - OpenGL Mathematics Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file glmath_vec3.h
 *	@date 07.10.2014
 *  @author Armon Carigiet
 *
 *  @brief Provides algebra and transformation functions for 3d vectors (glmath_vec3).
 *  @see glmath_types.h
 */
//===========================================================================

#ifndef glmathlib_glmath_vec3_h
#define glmathlib_glmath_vec3_h

#include "glmath_types.h"

#ifdef __cplusplus
extern "C" {
#endif

//! Builds a vector(3) from vector components.
//! @param x x vector component
//! @param y y vector component
//! @param z z vector component
//! @return vector
static inline glmath_vec3 glmath_vec3_create(glmath_real x, glmath_real y, glmath_real z);

//! Negates a vector(3).
//! @param v input vector
//! @return negated vector
static inline glmath_vec3 glmath_vec3_neg(glmath_vec3 v);

//! Adds two vectors(3).
//! @param v1 left vector
//! @param v2 right vector
//! @return resulting vector
static inline glmath_vec3 glmath_vec3_add(glmath_vec3 v1, glmath_vec3 v2);

//! Subtracts two vectors(3)
//! @param v1 left vector
//! @param v2 right vector
//! @return resulting vector
static inline glmath_vec3 glmath_vec3_sub(glmath_vec3 v1, glmath_vec3 v2);

//! Multiplicates two vectors(3).
//! @param v1 left vector
//! @param v2 right vector
//! @return resulting vector
static inline glmath_vec3 glmath_vec3_mul(glmath_vec3 v1, glmath_vec3 v2);

//! Divides two vectors(3).
//! @param v1 left vector
//! @param v2 right vector
//! @return resulting vector
static inline glmath_vec3 glmath_vec3_div(glmath_vec3 v1, glmath_vec3 v2);

//! Adds a scalar value to a vector(3).
//! @param v vector
//! @param a scalar value
//! @return resulting vector
static inline glmath_vec3 glmath_vec3_add_s(glmath_vec3 v, glmath_real a);

//! Subtracts a scalar value to a vector(3).
//! @param v vector
//! @param a scalar value
//! @return resulting vector
static inline glmath_vec3 glmath_vec3_sub_s(glmath_vec3 v, glmath_real a);

//! Multiplicates a vector(3) by a sclar value.
//! @param v vector
//! @param a scalar value
//! @return resulting vector
static inline glmath_vec3 glmath_vec3_mul_s(glmath_vec3 v, glmath_real a);

//! Divides a vector(3) by a sclar value.
//! @param v vector
//! @param a scalar value
//! @return resulting vector
static inline glmath_vec3 glmath_vec3_div_s(glmath_vec3 v, glmath_real a);

//! Calculates the magnitude of a vector(3).
//! @param v vector
//! @return magnitude
static inline glmath_real glmath_vec3_mag(glmath_vec3 v);

//! Calculates the distance between two vectors(3).
//! @param v1 first vector
//! @param v2 second vector
//! @return distance
static inline glmath_real glmath_vec3_dist(glmath_vec3 v1, glmath_vec3 v2);

//! Normalizes a vector(3).
//! @param v vector
//! @return normalized vector
static inline glmath_vec3 glmath_vec3_norm(glmath_vec3 v);

//! Calculates the dot-product of two vectors(3).
//! @param v1 first vector
//! @param v2 second vector
//! @return dot product
static inline glmath_real glmath_vec3_dot(glmath_vec3 v1, glmath_vec3 v2);

//! Calculates the cross-product of two vectors(3).
//! @param v1 left vector
//! @param v2 right vector
//! @return resulting vector
static inline glmath_vec3 glmath_vec3_cross(glmath_vec3 v1, glmath_vec3 v2);

//! Compares two vectors(3) and creates new one with the bigger components.
//! @param v1 first vector
//! @param v2 second vector
//! @return resulting vector
static inline glmath_vec3 glmath_vec3_max(glmath_vec3 v1, glmath_vec3 v2);

//! Compares two vectors(3) and creates new one with the smaller components.
//! @param v1 first vector
//! @param v2 second vector
//! @return resulting vector
static inline glmath_vec3 glmath_vec3_min(glmath_vec3 v1, glmath_vec3 v2);

//! Interpolates linear two vectors(3).
//! @note a should be in the interval ]0,1[
//! @param sv start vector
//! @param ev end vector
//! @param a interpolation value
//! @return resulting vector
static inline glmath_vec3 glmath_vec3_lerp(glmath_vec3 sv, glmath_vec3 ev, glmath_real a);

//! Projects a vector(3) onto another.
//! @param v vector to project
//! @param pv projection vector
//! @return projected vector
static inline glmath_vec3 glmath_vec3_proj(glmath_vec3 v, glmath_vec3 pv);

#include "glmath_vec3.inl"

#ifdef __cplusplus
}
#endif
	
#endif