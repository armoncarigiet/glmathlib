//===========================================================================
/*
 *  glmath - OpenGL Mathematics Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file glmath_vec2.h
 *	@date 06.10.2014
 *  @author Armon Carigiet
 *
 *  @brief Provides algebra and transformation functions for 2d vectors (glmath_vec2).
 *  @see glmath_types.h
 */
//===========================================================================

#ifndef glmathlib_glmath_vec2_h
#define glmathlib_glmath_vec2_h

#include "glmath_types.h"

#ifdef __cplusplus
extern "C" {
#endif

//! Builds a vector(2) from vector components.
//! @param x x vector component
//! @param y y vector component
//! @return vector
static inline glmath_vec2 glmath_vec2_create(glmath_real x, glmath_real y);

//! Negates a vector(2).
//! @param v input vector
//! @return negated vector
static inline glmath_vec2 glmath_vec2_neg(glmath_vec2 v);

//! Adds two vectors(2).
//! @param v1 left vector
//! @param v2 right vector
//! @return resulting vector
static inline glmath_vec2 glmath_vec2_add(glmath_vec2 v1, glmath_vec2 v2);

//! Subtracts two vectors(2)
//! @param v1 left vector
//! @param v2 right vector
//! @return resulting vector
static inline glmath_vec2 glmath_vec2_sub(glmath_vec2 v1, glmath_vec2 v2);

//! Multiplicates two vectors(2).
//! @param v1 left vector
//! @param v2 right vector
//! @return resulting vector
static inline glmath_vec2 glmath_vec2_mul(glmath_vec2 v1, glmath_vec2 v2);

//! Divides two vectors(2).
//! @param v1 left vector
//! @param v2 right vector
//! @return resulting vector
static inline glmath_vec2 glmath_vec2_div(glmath_vec2 v1, glmath_vec2 v2);

//! Adds a scalar value to a vector(2).
//! @param v vector
//! @param a scalar value
//! @return resulting vector
static inline glmath_vec2 glmath_vec2_add_s(glmath_vec2 v, glmath_real a);

//! Subtracts a scalar value to a vector(2).
//! @param v vector
//! @param a scalar value
//! @return resulting vector
static inline glmath_vec2 glmath_vec2_sub_s(glmath_vec2 v, glmath_real a);

//! Multiplicates a vector(2) by a scalar value.
//! @param v vector
//! @param a scalar value
//! @return resulting vector
static inline glmath_vec2 glmath_vec2_mul_s(glmath_vec2 v, glmath_real a);

//! Divides a vector(2) by a scalar value.
//! @param v vector
//! @param a scalar value
//! @return resulting vector
static inline glmath_vec2 glmath_vec2_div_s(glmath_vec2 v, glmath_real a);
	
//! Calculates the magnitude of a vector(2).
//! @param v vector
//! @return magnitude
static inline glmath_real glmath_vec2_mag(glmath_vec2 v);
	
//! Calculates the distance between two vectors(2).
//! @param v1 first vector
//! @param v2 second vector
//! @return distance
static inline glmath_real glmath_vec2_dist(glmath_vec2 v1, glmath_vec2 v2);
	
//! Normalizes a vector(2).
//! @param v vector
//! @return normalized vector
static inline glmath_vec2 glmath_vec2_norm(glmath_vec2 v);
	
//! Calculates the dot-product of two vectors(2).
//! @param v1 first vector
//! @param v2 second vector
//! @return dot product
static inline glmath_real glmath_vec2_dot(glmath_vec2 v1, glmath_vec2 v2);
	
//! Compares two vectors(2) and creates new one with the bigger components.
//! @param v1 first vector
//! @param v2 second vector
//! @return resulting vector
static inline glmath_vec2 glmath_vec2_max(glmath_vec2 v1, glmath_vec2 v2);
	
//! Compares two vectors(2) and creates new one with the smaller components.
//! @param v1 first vector
//! @param v2 second vector
//! @return resulting vecor
static inline glmath_vec2 glmath_vec2_min(glmath_vec2 v1, glmath_vec2 v2);
	
//! Interpolates linear two vectors(2).
//! @note a should be in the intervall ]0,1[
//! @param sv start vector
//! @param ev end vector
//! @param a interpolation skalar
//! @return resulting vector
static inline glmath_vec2 glmath_vec2_lerp(glmath_vec2 sv, glmath_vec2 ev, glmath_real a);
	
//! Projects a vector(2) onto another.
//! @param v vector to project
//! @param pv projection vector
//! @return projected vector
static inline glmath_vec2 glmath_vec2_proj(glmath_vec2 v, glmath_vec2 pv);

//! Calculates the vector which is perpendicular to the given.
//! @param v vector
//! @return prpendicular vector
static inline glmath_vec2 glmath_vec2_perp(glmath_vec2 v);

#include "glmath_vec2.inl"
	
#ifdef __cplusplus
}
#endif
	
#endif